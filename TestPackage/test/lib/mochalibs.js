const chai = require('chai');
const assert = require('chai').assert;
const should = require('chai').should();
const expect = require('chai').expect;
var chaiSubset = require('chai-subset');
const axios = require('axios');
const server = require('../utils/dbconnect');
const mongo = require('mongodb');
const ObjectId = require('mongodb').ObjectId;
const MUUID = require('uuid-mongodb');
var moment = require('moment');
const fs = require('fs');
const addContext = require('mochawesome/addContext');
const deepEqualInAnyOrder = require('deep-equal-in-any-order');
const deepEqual = require('deep-equal');
const chaiExclude = require('chai-exclude');
const formData = require('form-data');
chai.use(chaiExclude);
chai.use(chaiSubset);
chai.use(deepEqualInAnyOrder);
chai.use(deepEqual);

module.exports.mochalibs = {
   assert: assert,
   should: should,
   expect: expect,
   axios: axios,
   server: server,
   chai: chai,
   chaiSubset: chaiSubset,
   mongo: mongo,
   moment: moment,
   fs: fs,
   formData: formData,
   MUUID: MUUID,
   ObjectId: ObjectId,
   chaiExclude: chaiExclude
};