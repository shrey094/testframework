const MongoClient = require('mongodb').MongoClient;
const { clientKey, clientURI, centralURI } = require('../lib/dbconfig').dbconfig;
var client;
var central;

async function getDB() {
    try {
        client = new MongoClient("mongodb+srv://devadmin:thkFPTuoFs9cZWRb@developmentcluster.7mwxy.azure.mongodb.net/test?retryWrites=true&w=majority", { useNewUrlParser: true, useUnifiedTopology: true });
        return await client.connect()
            .then((connection) => connection.db(clientKey));
    } catch (e) {
        console.log("Not able to connect to database. Throwing error: " + e);
        throw e;
    }
}

async function getLocation() {

    try {
        central = new MongoClient("mongodb+srv://sshah:jkWDwOFGpBnlYNDb@centralcluster-frgmh.azure.mongodb.net/central?retryWrites=true&w=majority", { useNewUrlParser: true, useUnifiedTopology: true });
        let centralConn = await central.connect();
        let centralDb = await centralConn.db();
        let clientInfo = await centralDb.collection('clients').findOne({ clientKey: clientKey });
        if (!clientInfo) {
            console.log('Client not found.');
            return;
        };
        let clientLocation = clientInfo.location;
        //centralConn.close();
        return clientLocation;
    } catch (e) {
        console.log("Not able to connect to database. Throwing error: " + e);
        throw e;
    }
}

async function closeClient() {
    await client.close();
}

async function closeCentral() {
    await central.close();
}

module.exports.dbconnect = {
    getDB: getDB,
    getLocation: getLocation,
    closeCentral: closeCentral,
    closeClient: closeClient
}