const { expect, chaiSubset, chai, assert } = require('../lib/mochalibs').mochalibs;
chai.use(chaiSubset);

//To compare array of objects
function deepEqualInAnyOrder(actualList, expectedList) {
    expect(actualList).to.deep.equalInAnyOrder(expectedList);
}

function deepEqual(actualList, expectedList, messageOnFailure) {
    expect(actualList, messageOnFailure).to.deep.equal(expectedList);
}


function verifyContainSubset(actualObject, expectedObject, messageOnFailure) {
    expect(actualObject).to.containSubset(expectedObject, messageOnFailure);
}


function verifyEquals(actualValue, expectedValue, messageOnFailure) {
    expect(actualValue).to.equal(expectedValue, messageOnFailure);
}


function verifyCountFromDB(actualCount, expectedCount, messageOnFailure) {
    expect(actualCount).to.equal(expectedCount, messageOnFailure);
}

function verifyObjectPropertyByKeyAndValue(array, key, value) {
    array.forEach(function (element) {
        expect(element).to.have.property(key).to.equal(value);
    })
}

async function removePropFromJSON(jsonobject, propsToBeRemoved) {
    for (let index in propsToBeRemoved) {
        delete jsonobject[propsToBeRemoved[index]];
    }
    return jsonobject;
}

async function deepCompare(actualJson, expectedJson) {
    var lengthOfJson;
    var unMatchingKeys = [];
    // find keys
    var actualJsonKeys = Object.keys(actualJson);
    var expectedJsonKeys = Object.keys(expectedJson);

    // find values
    var actualJsonValues = Object.values(actualJson);
    var expectedJsonValues = Object.values(expectedJson);

    // find max length to iterate over all values in both jsons.
    actualJsonKeys.length > expectedJsonKeys.length ? lengthOfJson = actualJsonKeys.length : lengthOfJson = expectedJsonKeys.length;

    // Check if the objects are a match or not
    if (assert(JSON.stringify(actualJson), JSON.stringify(expectedJson))) {
        console.log("The json objects are a match.");
    } else {
        for (var i = 0; i < lengthOfJson; i++) {

            //Check if the keys are same
            if (actualJsonKeys[i] !== expectedJsonKeys[i]) {
                throw new Error(`um-matching Keys: at index ${i} \n Object 1: ${actualJsonKeys[i]} \n object 2: ${expectedJsonKeys[i]}`);
                //if the keys are  same compare values
            } else {
                if (actualJsonValues[i] !== expectedJsonValues[i]) {
                    console.log(`The value of ${actualJsonKeys[i]} doesn't match.\n Object 1: ${actualJsonValues[i].toString()} \n object 2: ${expectedJsonValues[i].toString()}`);
                    unMatchingKeys.push(actualJsonKeys[i])
                }
            }
        }
        throw new Error(`unmatching keys: ${unMatchingKeys}`)
    }
}
module.exports.mochautils = {

    deepEqualInAnyOrder: deepEqualInAnyOrder,
    deepEqual: deepEqual,
    verifyEquals: verifyEquals,
    verifyObjectPropertyByKeyAndValue: verifyObjectPropertyByKeyAndValue,
    removePropFromJSON: removePropFromJSON,
    deepCompare: deepCompare

};