const { chaiSubset, chai } = require('../lib/mochalibs').mochalibs;
const { getDB, getLocation } = require('../utils/dbconnect').dbconnect;
chai.use(chaiSubset);
//To find one database entry from mongodb and returns the entry
async function findOne(collectionName, query, projectionObject) {
    //let location = await getLocation();
    validateCollection(collectionName)
    let result = (await getDB()).collection(collectionName).findOne({ ...query }, { projection: projectionObject });
    return result;
}
//To find multiple database entry from mongodb and return the entries
async function find(collectionName, query, projectionObject, sortBy) {
    let location = await getLocation();
    let db = await getDB()
    validateCollection(collectionName)
    return await (await getDB()).collection(collectionName).find({ "location": location, ...query }, { projection: projectionObject }).sort(sortBy).toArray();
}
//To insert one object in a collection. query should be an object that you want to insert 
async function insertOne(collectionName, query) {
    let db = await getDB()
    validateCollection(collectionName)
    return await (await getDB()).collection(collectionName).insertOne(query);
}
//To insert multiple objects in a collection. query should be array of objects that you want to insert.
async function insertMany(collectionName, query) {
    let db = await getDB()
    validateCollection(collectionName)
    return await (await getDB()).collection(collectionName).insertMany(query);
}
//To find count database entry from mongodb and returns the count
async function count(collectionName, query) {
    let location = await getLocation();
    validateCollection(collectionName)
    return (await getDB()).collection(collectionName).countDocuments({ "location": location, ...query });
}
//To delete one database entry from mongodb and return the count of the deleted item
async function deleteOne(collectionName, query) {
    validateCollection(collectionName)
    const db = await getDB()
    await db.collection(collectionName).deleteOne({ ...query });
    return db.collection(collectionName).countDocuments({ ...query });
}
//To delete many database entries from mongodb and return the count of the deleted item
async function deleteMany(collectionName, query) {
    let location = await getLocation();
    validateCollection(collectionName)
    await (await getDB()).collection(collectionName).deleteMany({ "location": location, ...query });
    return (await getDB()).collection(collectionName).countDocuments({ "location": location, ...query });
}
//To validate if the collection exists or not 
async function validateCollection(collectionName) {
    let db = await getDB()
    let listcollection = await db.listCollections().toArray()
    var collections = [];
    for (var i = 0; i < listcollection.length; ++i)
        collections.push(listcollection[i].name);
    if (collections.indexOf(collectionName) < 0) {
        throw new Error('Collection does not exist');
    }
}
async function updateFieldUsingOtherField(collectionName, query, uniqueNode, source, target) {
    /*  let db = await server.getDb();
     let location = await getLocation();
     await (await getDB()).collection(collectionName).find({"location": location, ...query}).forEach(
         function (elem) {
             await (await getDB()).collection(collectionName).update(
                 {
                     uniqueNode:  elem.uniqueNode
                 },
                 {
                     $set: {
                         target: elem.source
                     }
                 }
             );
         }
     ); */
}
module.exports.dbwrapper = {
    findOne: findOne,
    find: find,
    count: count,
    deleteOne: deleteOne,
    deleteMany: deleteMany,
    insertMany: insertMany,
    insertOne: insertOne,
    updateFieldUsingOtherField: updateFieldUsingOtherField,
    validateCollection: validateCollection
};