var startDate = "01/19/2021";
var startDateResponse = "2021-01-19T00:00:00.000Z";

var startOn = "01/20/2021";
var startOnResponse = "2021-01-20T00:00:00.000Z";

var dueDateResponse = "01/26/2021";
var dueDate = "2021-01-26T00:00:00.000Z"

var endDate = "11/25/2021";
var endDateResponse = "2021-11-25T00:00:00.000Z";

var endOn = "11/25/2021";
var endOnResponse = "2021-11-25T00:00:00.000Z";

var createTask = {
    Request: {
        "allParameters": {
            "_objectType": "task",
            "recurrence": {
                "type": "none"
            },
            "attachment": [

            ],
            "name": "test",
            "category": "nerc",
            "startDate": startDate,
            "dueDate": dueDate,
            "description": null,
            "priority": "low",
            "status": "open",
            "impact": "indirect",
            "assignedTo": null,
            "notificationDays": null,
            "completionDate": null,
            "links": [{
                "_id": "5ff87b8c461ae739f4aa5c3e",
                "_objectType": "contract"
            }],
            "tags": [],
            "securityTags": [

            ]
        },


        "mandatoryFields": {
            "_objectType": "task",
            "recurrence": {
                "type": "none"
            },
            "attachment": [

            ],
            "name": "test",
            "startDate": startDate,
            "dueDate": dueDate,
            "status": "open",
            "links": [{
                "_id": "",
                "_objectType": "contract"
            }]
        },


        "withProjectLinks": {
            "_objectType": "task",
            "recurrence": {
                "type": "none"
            },
            "attachment": [

            ],
            "name": "test",
            "category": "nerc",
            "startDate": startDate,
            "dueDate": dueDate,
            "description": null,
            "priority": "low",
            "status": "open",
            "impact": "indirect",
            "assignedTo": null,
            "notificationDays": null,
            "completionDate": null,
            "links": [{
                "_id": "",
                "_objectType": "contract"
            },
            {
                "_id": "",
                "_objectType": "project"
            }
            ],
            "tags": [

            ],
            "securityTags": [

            ]
        },


        "withProjectGroup": {
            "_objectType": "task",
            "recurrence": {
                "type": "none"
            },
            "attachment": [

            ],
            "name": "test",
            "startDate": startDate,
            "dueDate": dueDate,
            "status": "open",
            "links": [{
                "_id": "",
                "_objectType": "contract"
            },
            {
                "_objectType": "projectGroup",
                "_id": ""
            }
            ]
        },


        "assignToNone": {
            "_objectType": "task",
            "recurrence": {
                "type": "none"
            },
            "attachment": [

            ],
            "name": "test",
            "category": "nerc",
            "startDate": startDate,
            "dueDate": dueDate,
            "description": null,
            "priority": "low",
            "status": "open",
            "impact": "indirect",
            "assignedTo": null,
            "notificationDays": null,
            "completionDate": null,
            "links": [{
                "_id": "",
                "_objectType": "contract"
            }],
            "tags": [

            ],
            "securityTags": [

            ]
        },


        "dailyRecurrence": {
            "_objectType": "task",
            "recurrence": {
                "type": "daily",
                "startOn": startOn,
                "duration": 1,
                "every": 10,
                "everyType": "day",
                "onweekDay": null,
                "onDate": null,
                "onMonth": null,
                "endOn": endOn
            },
            "attachment": [

            ],
            "name": "test",
            "category": null,
            "startDate": startDate,
            "dueDate": dueDate,
            "description": null,
            "priority": "low",
            "status": "open",
            "overdueImpact": "at-risk",
            "assignedTo": null,
            "notificationDays": null,
            "completionDate": null,
            "links": [{
                "_id": "",
                "_objectType": "contract"
            }],
            "tags": [

            ],
            "securityTags": [

            ]
        },


        "weeklyRecurrence": {
            "_objectType": "task",
            "recurrence": {
                "type": "weekly",
                "startOn": startOn,
                "duration": 1,
                "every": 1,
                "everyType": "week",
                "onweekDay": [
                    "Tuesday"
                ],
                "onDate": null,
                "onMonth": null,
                "endOn": endOn
            },
            "attachment": [

            ],
            "name": "test",
            "category": null,
            "startDate": startDate,
            "dueDate": dueDate,
            "description": null,
            "priority": "low",
            "status": "open",
            "overdueImpact": "at-risk",
            "assignedTo": null,
            "notificationDays": null,
            "completionDate": null,
            "links": [{
                "_id": "5ffdba040f4d9320d851c261",
                "_objectType": "contract"
            }],
            "tags": [

            ],
            "securityTags": [

            ]
        },


        "monthlyRecurrence": {
            "_objectType": "task",
            "recurrence": {
                "type": "monthly",
                "startOn": startOn,
                "duration": 1,
                "every": 1,
                "everyType": "month",
                "onweekDay": null,
                "onDate": 12,
                "onMonth": null,
                "endOn": endOn

            },
            "attachment": [

            ],
            "name": "test",
            "category": null,
            "startDate": startDate,
            "dueDate": dueDate,
            "description": null,
            "priority": "low",
            "status": "open",
            "overdueImpact": "at-risk",
            "assignedTo": null,
            "notificationDays": null,
            "completionDate": null,
            "links": [{
                "_id": "",
                "_objectType": "contract"
            }],
            "tags": [

            ],
            "securityTags": [

            ]
        },


        "yearlyRecurrence": {
            "_objectType": "task",
            "recurrence": {
                "type": "yearly",
                "startOn": startOn,
                "duration": 1,
                "every": 1,
                "everyType": "year",
                "onweekDay": null,
                "onDate": 12,
                "onMonth": "January",
                "endOn": endOn
            },
            "attachment": [

            ],
            "name": "test",
            "category": null,
            "startDate": startDate,
            "dueDate": dueDate,
            "description": null,
            "priority": "low",
            "status": "open",
            "overdueImpact": "at-risk",
            "assignedTo": null,
            "notificationDays": null,
            "completionDate": null,
            "links": [{
                "_id": "5ffdba040f4d9320d851c261",
                "_objectType": "contract"
            }],
            "tags": [

            ],
            "securityTags": [

            ]
        }
    },
    Response: {
        "allParameters": {
            "_id": "5ff87bce461ae739f4aa5c48",
            "_objectType": "task",
            "recurrence": {
                "type": "none"
            },
            "attachment": [

            ],
            "name": "test",
            "category": "nerc",
            "startDate": startDateResponse,
            "dueDate": dueDate,
            "description": null,
            "priority": "low",
            "status": "open",
            "impact": "indirect",
            "assignedTo": [

            ],
            "notificationDays": null,
            "completionDate": null,
            "links": [{
                "_id": "5ff87b8c461ae739f4aa5c3e",
                "_objectType": "contract",
                "name": "Test-Operations and Maintenance Agreement",
                "active": true,
                "type": "contract",
                "startDate": startDateResponse,
                "endDate": endDateResponse,
                "_relatedTo": "5ff87bce461ae739f4aa5c48",
                "_isDirect": true,
                "displayText": "Test-Operations and Maintenance Agreement"
            }],
            "tags": [

            ],
            "securityTags": [

            ]
        },
        "mandatoryFields": {
            "_id": "5ff880b6461ae739f4aa5d31",
            "_objectType": "task",
            "recurrence": {
                "type": "none"
            },
            "attachment": [

            ],
            "name": "test",
            "startDate": startDateResponse,
            "dueDate": dueDate,
            "status": "open",
            "links": [{
                "_id": "5ff87b8c461ae739f4aa5c3e",
                "_objectType": "contract",
                "name": "Test-Operations and Maintenance Agreement",
                "active": true,
                "type": "contract",
                "startDate": startDateResponse,
                "endDate": endDateResponse,
                "_relatedTo": "5ff87bce461ae739f4aa5c48",
                "_isDirect": true,
                "displayText": "Test-Operations and Maintenance Agreement"
            }],
            "securityTags": [

            ],
            "assignedTo": [

            ],
            "description": null,
            "category": null,
            "priority": "low",
            "completionDate": null,
            "notificationDays": null,
            "tags": null
        },
        "withProjectLinks": {
            "_id": "5ff88c76461ae739f4aa5f6a",
            "_objectType": "task",
            "recurrence": {
                "type": "none"
            },
            "attachment": [

            ],
            "name": "test",
            "category": "nerc",
            "startDate": startDateResponse,
            "dueDate": dueDateResponse,
            "description": null,
            "priority": "low",
            "status": "open",
            "impact": "indirect",
            "assignedTo": [

            ],
            "notificationDays": null,
            "completionDate": null,
            "links": [{
                "_id": "5ff88c74461ae739f4aa5f5f",
                "_objectType": "project",
                "name": "test",
                "projectKey": "projectcreate-pssg6",
                "_relatedTo": "5ff88c76461ae739f4aa5f6a",
                "_isDirect": true
            },
            {
                "_id": "5ff87b8c461ae739f4aa5c3e",
                "_objectType": "contract",
                "name": "Test-Operations and Maintenance Agreement",
                "active": true,
                "type": "contract",
                "startDate": startDateResponse,
                "endDate": endDateResponse,
                "_relatedTo": "5ff87bce461ae739f4aa5c48",
                "_isDirect": true,
                "displayText": "Test-Operations and Maintenance Agreement"
            }
            ],
            "tags": [

            ],
            "securityTags": [

            ]
        },
        "withProjectGroup": {
            "_id": "5ffc5d6a068d0f2fc07f6f7c",
            "_objectType": "task",
            "recurrence": {
                "type": "none"
            },
            "attachment": [

            ],
            "name": "test",
            "category": null,
            "startDate": startDateResponse,
            "dueDate": dueDate,
            "description": null,
            "priority": "low",
            "status": "open",
            "assignedTo": [

            ],
            "notificationDays": null,
            "completionDate": null,
            "links": [{
                "_id": "5ffdc8b2bd5d320ab0edd88f",
                "_isDirect": true,
                "_objectType": "projectGroup",
                "_relatedTo": "5ffdc8b4bd5d320ab0edd896",
                "name": "test1234d",
                "projectGroupKey": "project_group_create-2298m"
            },
            {
                "_id": "5ff87b8c461ae739f4aa5c3e",
                "_objectType": "contract",
                "name": "Test-Operations and Maintenance Agreement",
                "active": true,
                "type": "contract",
                "startDate": startDateResponse,
                "endDate": endDateResponse,
                "_relatedTo": "5ff87bce461ae739f4aa5c48",
                "_isDirect": true,
                "displayText": "Test-Operations and Maintenance Agreement"
            }
            ],
            "tags": null,
            "securityTags": [

            ]
        },
        "assignToNone": {
            "_id": "5ff87bce461ae739f4aa5c48",
            "_objectType": "task",
            "recurrence": {
                "type": "none"
            },
            "attachment": [

            ],
            "name": "test",
            "category": "nerc",
            "startDate": startDateResponse,
            "dueDate": dueDate,
            "description": null,
            "priority": "low",
            "status": "open",
            "impact": "indirect",
            "assignedTo": [

            ],
            "notificationDays": null,
            "completionDate": null,
            "links": [{
                "_id": "5ff87b8c461ae739f4aa5c3e",
                "_objectType": "contract",
                "name": "Test-Operations and Maintenance Agreement",
                "active": true,
                "type": "contract",
                "startDate": startDateResponse,
                "endDate": endDateResponse,
                "_relatedTo": "5ff87bce461ae739f4aa5c48",
                "_isDirect": true,
                "displayText": "Test-Operations and Maintenance Agreement"
            }],
            "tags": [

            ],
            "securityTags": [

            ]
        },
        "dailyRecurrence": {
            "_objectType": "task",
            "recurrence": {
                "type": "daily",
                "duration": 1,
                "every": 1,
                "everyType": "day",
                "onweekDay": null,
                "onDate": null,
                "onMonth": null,
                "endOn": endOnResponse,
                "recurrenceId": "5ffdd554bd5d320ab0ef5525",
                "startOn": startOnResponse,
                "inSync": true
            },
            "attachment": [

            ],
            "name": "test",
            "overdueImpact": "at-risk",
            "category": null,
            "startDate": startDateResponse,
            "dueDate": dueDate,
            "description": null,
            "priority": "low",
            "status": "open",
            "assignedTo": [

            ],
            "notificationDays": null,
            "completionDate": null,
            "links": [{
                "_id": "5ff87b8c461ae739f4aa5c3e",
                "_objectType": "contract",
                "name": "Test-Operations and Maintenance Agreement",
                "active": true,
                "type": "contract",
                "startDate": startDateResponse,
                "endDate": endDateResponse,
                "_relatedTo": "5ff87bce461ae739f4aa5c48",
                "_isDirect": true,
                "displayText": "Test-Operations and Maintenance Agreement"
            }],
            "tags": [

            ],
            "securityTags": [

            ],
            "_id": "5ffc81a6068d0f2fc07f758a"
        },
        "weeklyRecurrence": {
            "_objectType": "task",
            "recurrence": {
                "type": "weekly",
                "startOn": startOnResponse,
                "duration": 1,
                "every": 1,
                "everyType": "week",
                "onweekDay": [
                    "Tuesday"
                ],
                "onDate": null,
                "onMonth": null,
                "endOn": endOnResponse,
                "recurrenceId": "5ffddb45bd5d320ab0ef7d26",
                "inSync": true
            },
            "attachment": [

            ],
            "name": "test",
            "category": null,
            "startDate": "2021-01-26T00:00:00.000Z",
            "dueDate": dueDate,
            "description": null,
            "priority": "low",
            "status": "open",
            "overdueImpact": "at-risk",
            "assignedTo": [

            ],
            "notificationDays": null,
            "completionDate": null,
            "links": [{
                "_id": "5ff87b8c461ae739f4aa5c3e",
                "_objectType": "contract",
                "name": "Test-Operations and Maintenance Agreement",
                "active": true,
                "type": "contract",
                "startDate": "2021-01-19T00:00:00.000Z",
                "endDate": endDateResponse,
                "_relatedTo": "5ff87bce461ae739f4aa5c48",
                "_isDirect": true,
                "displayText": "Test-Operations and Maintenance Agreement"
            }],
            "tags": [

            ],
            "securityTags": [

            ],
            "_id": "5ffddb45bd5d320ab0ef7dd8"
        },
        "monthlyRecurrence": {
            "_objectType": "task",
            "recurrence": {
                "type": "monthly",
                "startOn": startOnResponse,
                "duration": 1,
                "every": 1,
                "everyType": "month",
                "onweekDay": null,
                "onDate": 12,
                "onMonth": null,
                "endOn": endOnResponse,
                "recurrenceId": "5ffddee80f4d9320d85219c0",
                "inSync": true
            },
            "attachment": [

            ],
            "name": "test",
            "category": null,
            "startDate": "2021-02-12T00:00:00.000Z",
            "dueDate": "2021-02-12T00:00:00.000Z",
            "description": null,
            "priority": "low",
            "status": "open",
            "overdueImpact": "at-risk",
            "assignedTo": [

            ],
            "notificationDays": null,
            "completionDate": null,
            "links": [{
                "_id": "5ff87b8c461ae739f4aa5c3e",
                "_objectType": "contract",
                "name": "Test-Operations and Maintenance Agreement",
                "active": true,
                "type": "contract",
                "startDate": startDateResponse,
                "endDate": endDateResponse,
                "_relatedTo": "5ff87bce461ae739f4aa5c48",
                "_isDirect": true,
                "displayText": "Test-Operations and Maintenance Agreement"
            }],
            "tags": [

            ],
            "securityTags": [

            ],
            "_id": "5ffddb45bd5d320ab0ef7dd8"
        },
        "yearlyRecurrence": {
            "_objectType": "task",
            "recurrence": {
                "type": "yearly",
                "startOn": startOnResponse,
                "duration": 1,
                "every": 1,
                "everyType": "year",
                "onweekDay": null,

                "onDate": 12,
                "onMonth": "January",
                "endOn": endOnResponse,
                "recurrenceId": "5ffde2530f4d9320d8521f1f",
                "inSync": true
            },
            "attachment": [

            ],
            "name": "test",
            "category": null,
            "startDate": startDateResponse,
            "dueDate": dueDate,
            "description": null,
            "priority": "low",
            "status": "open",
            "overdueImpact": "at-risk",
            "assignedTo": [

            ],
            "notificationDays": null,
            "completionDate": null,
            "links": [{
                "_id": "5ff87b8c461ae739f4aa5c3e",
                "_objectType": "contract",
                "name": "Test-Operations and Maintenance Agreement",
                "active": true,
                "type": "contract",
                "startDate": startDateResponse,
                "endDate": endDateResponse,
                "_relatedTo": "5ff87bce461ae739f4aa5c48",
                "_isDirect": true,
                "displayText": "Test-Operations and Maintenance Agreement"
            }],
            "tags": [

            ],
            "securityTags": [

            ],
            "_id": "5ffddb45bd5d320ab0ef7dd8"
        }
    }
};
var updateTask = {
    Request: {
        "updateName_Description_NotificationDays": {
            "_id": "5ffe0c400f4d9320d852b342",
            "_objectType": "task",
            "recurrence": {
                "type": "none"
            },
            "attachment": [

            ],
            "name": "2",
            "category": null,
            "startDate": startDate,
            "dueDate": dueDate,
            "description": "updated description",
            "priority": "low",
            "status": "open",
            "overdueImpact": "none",
            "assignedTo": null,
            "notificationDays": 2,
            "completionDate": null,
            "links": [{
                "_id": "5ffe0c0c0f4d9320d852b15b",
                "_objectType": "contract",
                "_relatedTo": "5ffe0c400f4d9320d852b342"
            }],
            "tags": [

            ],
            "securityTags": [

            ],
            "updateTaskSeries": false
        },
        "Update_Category_status_assignedTo_Priority": {
            "_id": "5ffe0c400f4d9320d852b342",
            "_objectType": "task",
            "recurrence": {
                "type": "none"
            },
            "attachment": [

            ],
            "name": "test",
            "category": "nerc",
            "priority": "high",
            "startDate": startDate,
            "dueDate": dueDate,
            "description": null,
            "status": "closed",
            "overdueImpact": "none",
            "assignedTo": [{
                "_id": "5fff1c0029f03221a070abd4",
                "_objectType": "contact"
            }],
            "notificationDays": null,
            "completionDate": null,
            "links": [{
                "_id": "5ffe0c0c0f4d9320d852b15b",
                "_objectType": "contract",
                "_relatedTo": "5ffe0c400f4d9320d852b342"
            }],
            "tags": [

            ],
            "securityTags": [

            ],
            "updateTaskSeries": false
        },
        "Update_startEndCompletion_date": {
            "_id": "5ffe0c400f4d9320d852b342",
            "_objectType": "task",
            "recurrence": {
                "type": "none"
            },
            "attachment": [

            ],
            "name": "2",
            "category": null,
            "startDate": startDate,
            "dueDate": dueDate,
            "description": "updated description",
            "priority": "low",
            "status": "open",
            "overdueImpact": "none",
            "assignedTo": null,
            "notificationDays": null,
            "completionDate": "02/10/2022",
            "links": [{
                "_id": "5ffe0c0c0f4d9320d852b15b",
                "_objectType": "contract",
                "_relatedTo": "5ffe0c400f4d9320d852b342"
            }],
            "tags": [

            ],
            "securityTags": [

            ],
            "updateTaskSeries": false
        },
        "update_monthlyRecurrenceEndDate": {
            "_id": "",
            "_objectType": "task",
            "recurrence": {
                "type": "monthly",
                "startOn": startOnResponse,
                "duration": 1,
                "every": 1,
                "everyType": "month",
                "onweekDay": null,
                "onDate": 13,
                "onMonth": null,
                "endOn": endOnResponse,
                "recurrenceId": "5fff44b50f4d9320d8539922",
                "inSync": false
            },
            "attachment": [

            ],
            "name": "shrey test-1",
            "category": "nerc",
            "startDate": startDate,
            "dueDate": dueDate,
            "description": null,
            "priority": "low",
            "status": "open",
            "overdueImpact": "at-risk",
            "assignedTo": null,
            "notificationDays": null,
            "completionDate": null,
            "links": [{
                "_id": "",
                "_objectType": "contract",
                "_relatedTo": ""
            }],
            "tags": [

            ],
            "securityTags": [

            ],
            "updateTaskSeries": true
        },
        "update_dailyRecurrenceStartDate": {
            "_id": "",
            "_objectType": "task",
            "recurrence": {
                "type": "daily",
                "duration": 1,
                "every": 1,
                "everyType": "day",
                "onweekDay": null,
                "onDate": null,
                "onMonth": null,
                "endOn": endOnResponse,
                "recurrenceId": "5ffdd554bd5d320ab0ef5525",
                "startOn": startOnResponse,
                "inSync": true
            },
            "attachment": [

            ],
            "name": "test",
            "category": "nerc",
            "startDate": startDate,
            "dueDate": dueDate,
            "description": null,
            "priority": "low",
            "status": "open",
            "overdueImpact": "at-risk",
            "assignedTo": null,
            "notificationDays": null,
            "completionDate": null,
            "links": [{
                "_id": "5fff4d9a29f03221a070c11f",
                "_objectType": "contract",
                "_relatedTo": ""
            }],
            "tags": [

            ],
            "securityTags": [

            ],
            "updateTaskSeries": true
        }
    },
    Response: {
        "updateName_Description_NotificationDays": {
            "_id": "5fff097229f03221a070a9f5",
            "_objectType": "task",
            "recurrence": {
                "type": "none"
            },
            "attachment": [

            ],
            "name": "2",
            "startDate": startDateResponse,
            "dueDate": dueDate,
            "status": "open",
            "links": [{
                "_id": "5fff097129f03221a070a9ef",
                "_objectType": "contract",
                "name": "Test-Operations and Maintenance Agreement",
                "active": true,
                "type": "contract",
                "startDate": startDateResponse,
                "endDate": endDateResponse,
                "_relatedTo": "5fff097229f03221a070a9f5",
                "displayText": "Test-Operations and Maintenance Agreement"
            }],
            "securityTags": [

            ],
            "assignedTo": [

            ],
            "description": "updated description",
            "category": null,
            "priority": "low",
            "completionDate": null,
            "notificationDays": 2,
            "tags": [

            ],
            "overdueImpact": "none",
            "updateTaskSeries": false
        },
        "Update_Category_status_assignedTo_Priority": {
            "_id": "5fff1ab629f03221a070ab3a",
            "_objectType": "task",
            "recurrence": {
                "type": "none"
            },
            "attachment": [

            ],
            "name": "test",
            "startDate": startDateResponse,
            "dueDate": dueDate,
            "status": "closed",
            "links": [{
                "_id": "5fff1ab229f03221a070ab34",
                "_objectType": "contract",
                "name": "Test-Operations and Maintenance Agreement",
                "active": true,
                "type": "contract",
                "startDate": startDateResponse,
                "endDate": endDateResponse,
                "_relatedTo": "5fff1ab629f03221a070ab3a",
                "displayText": "Test-Operations and Maintenance Agreement"
            }],
            "securityTags": [

            ],
            "assignedTo": [{
                "_id": "",
                "_objectType": "contact",
                "name": "Test1234 Shah"
            }],
            "description": null,
            "category": "nerc",
            "priority": "high",
            "completionDate": null,
            "notificationDays": null,
            "tags": [

            ],
            "overdueImpact": "none",
            "updateTaskSeries": false
        },
        "Update_startEndCompletion_date": {
            "_id": "5fff2e3929f03221a070adce",
            "_objectType": "task",
            "recurrence": {
                "type": "none"
            },
            "attachment": [

            ],
            "name": "2",
            "startDate": startDateResponse,
            "dueDate": dueDate,
            "status": "open",
            "links": [{
                "_id": "5fff2e3629f03221a070adc8",
                "_objectType": "contract",
                "name": "Test-Operations and Maintenance Agreement",
                "active": true,
                "type": "contract",
                "startDate": startDateResponse,
                "endDate": endDateResponse,
                "_relatedTo": "5fff2e3929f03221a070adce",
                "displayText": "Test-Operations and Maintenance Agreement"
            }],
            "securityTags": [

            ],
            "assignedTo": [

            ],
            "description": "updated description",
            "category": null,
            "priority": "low",
            "completionDate": "2022-02-10T00:00:00.000Z",
            "notificationDays": null,
            "tags": [

            ],
            "overdueImpact": "none",
            "updateTaskSeries": false
        },
        "update_monthlyRecurrenceEndDate": {
            "_id": "5fff4d9c29f03221a070c125",
            "_objectType": "task",
            "recurrence": {
                "type": "monthly",
                "startOn": startOnResponse,
                "duration": 1,
                "every": 1,
                "everyType": "month",
                "onweekDay": null,
                "onDate": 13,
                "onMonth": null,
                "endOn": endOnResponse,
                "recurrenceId": "5fff44b50f4d9320d8539922",
                "inSync": true
            },
            "attachment": [

            ],
            "name": "shrey test-1",
            "startDate": startDateResponse,
            "dueDate": dueDate,
            "status": "open",
            "links": [{
                "_id": "5fff4d9a29f03221a070c11f",
                "_objectType": "contract",
                "_relatedTo": "5fff4d9c29f03221a070c125",
                "active": true,
                "displayText": "Test-Operations and Maintenance Agreement",
                "endDate": endDateResponse,
                "name": "Test-Operations and Maintenance Agreement",
                "startDate": startDateResponse,
                "type": "contract"
            }],
            "securityTags": [

            ],
            "assignedTo": [

            ],
            "description": null,
            "category": "nerc",
            "priority": "low",
            "completionDate": null,
            "notificationDays": null,
            "tags": [

            ],
            "overdueImpact": "at-risk",
            "updateTaskSeries": true
        },
        "update_dailyRecurrenceStartDate": {
            "_id": "5ffdd554bd5d320ab0ef5b20",
            "_objectType": "task",
            "recurrence": {
                "type": "daily",
                "startOn": startOnResponse,
                "duration": 1,
                "every": 1,
                "everyType": "day",
                "onweekDay": null,
                "onDate": null,
                "onMonth": null,
                "endOn": endOnResponse,
                "recurrenceId": "5ffdd554bd5d320ab0ef5525",
                "inSync": true
            },
            "attachment": [

            ],
            "name": "test",
            "category": "nerc",
            "startDate": startDateResponse,
            "dueDate": dueDate,
            "description": null,
            "priority": "low",
            "status": "open",
            "overdueImpact": "at-risk",
            "assignedTo": [

            ],
            "notificationDays": null,
            "completionDate": null,
            "links": [{
                "_id": "5ffdd552bd5d320ab0ef5523",
                "_objectType": "contract",
                "name": "Test-Operations and Maintenance Agreement",
                "active": true,
                "type": "contract",
                "startDate": startDateResponse,
                "endDate": endDateResponse,
                "_relatedTo": "5ffdd554bd5d320ab0ef5b20",
                "displayText": "Test-Operations and Maintenance Agreement"
            }],
            "tags": [

            ],
            "securityTags": [

            ]
        }
    }
};
var retrieveTask = {
    Request: {
        "retrieveById": {
            "_id": "5fbea1eec82a6e046414f3db",
            "type": "task"
        }
    },
    Response: {
        "retrieveById": {
            "_id": "600404150f4d9320d85ef5cb",
            "_objectType": "task",
            "recurrence": {
                "type": "none"
            },
            "attachment": [

            ],
            "name": "test",
            "startDate": startDateResponse,
            "dueDate": dueDate,
            "status": "open",
            "links": [{
                "_id": "600404140f4d9320d85ef5c5",
                "_objectType": "contract",
                "name": "Test-Operations and Maintenance Agreement",
                "active": true,
                "type": "contract",
                "startDate": startDateResponse,
                "endDate": endDateResponse,
                "_relatedTo": "600404150f4d9320d85ef5cb",
                "_isDirect": true,
                "displayText": "Test-Operations and Maintenance Agreement"
            }],
            "securityTags": [

            ],
            "assignedTo": [

            ],
            "description": null,
            "category": null,
            "priority": "low",
            "completionDate": null,
            "notificationDays": null,
            "tags": null
        }
    }
};

var createProject = {
    input: {
        "_objectType": "project",
        "name": "test",
        "projectKey": "test",
        "type": "biomass",
        "size": {
            "value": null,
            "unit": "kW"
        },
        "startDate": startDate,
        "status": null,
        "description": null,
        "address": {
            "addressLine": null,
            "city": null,
            "postalCode": null,
            "state": null,
            "country": null
        },
        "geoLocation": {
            "type": "Point",
            "coordinates": [

            ]
        },
        "siteInfo": {
            "siteAccess": {
                "lessor": [

                ]
            },
            "siteOwnership": {
                "owner": [

                ]
            }
        },
        "businessDays": [
            "monday",
            "tuesday",
            "wednesday",
            "thursday",
            "friday"
        ],
        "links": [

        ]
    },
    expected: {
        "_objectType": "project",
        "projectKey": "test",
        "name": "Test Project Create",
        "description": "Create Works!",
        "type": "solar",
        "startDate": startDateResponse,
        "address": {
            "addressLine": "390 Ontario Cir.",
            "city": "Boston",
            "postalCode": "02130",
            "state": "Massachusetts",
            "country": "US"
        },
        "geoLocation": {
            "type": "Point",
            "coordinates": [1, 2]
        },
        "size": {
            "value": 2500,
            "unit": "kW"
        },
        "keyContact": [{
            "_objectType": "contact",
            "_id": "5ed918121896bfd3b3f85725",
            "name": "Linda Leader"
        }],
        "owner": [{
            "_objectType": "contact",
            "_id": "5ed9195c1896bfd3b3f8573b",
            "name": "Solra Power, Inc."
        }],
        "omOperator": [{
            "_objectType": "contact",
            "_id": "5ed91a4c1896bfd3b3f8574d",
            "name": "Warm Sun O&M"
        }],
        "interconnection": [{
            "_objectType": "contact",
            "_id": "5ed91a581896bfd3b3f8574e",
            "name": "Summer InterConnectors"
        }],
        "utility": [{
            "_objectType": "contact",
            "_id": "5ed91a661896bfd3b3f8574f",
            "name": "Eversource"
        }],
        "offtaker": [{
            "_objectType": "contact",
            "_id": "5ed91a741896bfd3b3f85750",
            "name": "Tradeenergy America"
        }],
        "status": "operational",
        "businessDays": ["monday", "tuesday"],
        "financialYearEnd": 12,
        "attachment": [],
        "timezone": {
            "region": null,
            "city": null,
            "databaseName": null
        },
        "siteInfo": {
            "siteAccess": {
                "instructions": "Access to the site is through the entrance located at South-East corner. Gate Access Code: 94413",
                "lessor": [{
                    "_id": "5ed9195c1896bfd3b3f8573b",
                    "_objectType": "contact",
                    "name": "Solra Power, Inc."
                }],
                "leaseTermMonth": 60,
                "leaseStartDate": "2016-04-01T04:00:00.000Z",
                "leaseEndDate": "2021-03-31T04:00:00.000Z"
            },
            "siteOwnership": {
                "owner": [{
                    "_id": "5ed9195c1896bfd3b3f8573b",
                    "_objectType": "contact",
                    "name": "Solra Power, Inc."
                }],
                "securityGateCode": "94413"
            }
        },
        "links": []
    }
};

var createProjectGroup = {
    Request: {
        allParameters: {
            "_objectType": "projectGroup",
            "projectGroupKey": "test1234d",
            "name": "test1234d"
        }
    },
    Response: {
        allParameters: {
            "_id": "5fe0f59ad85dc70f547044cc",
            "_objectType": "projectGroup",
            "projectGroupKey": "test1234d",
            "name": "test1234d",
            "_count": {},
            "links": []
        }
    }
}

var createContact = {

    validRequest: {

        allParameters: {
            "_objectType": "contact",
            "type": "individual",
            "name": {
                "firstName": "Test1234",
                "lastName": "Shah",
                "fullName": "Test1234 Shah"
            },
            "groups": [],
            "company": [],
            "isGlobal": false,
            "position": null,
            "address": {
                "addressLine": "390 Ontario Cir.",
                "city": "Boston",
                "postalCode": "02130",
                "state": "Massachusetts",
                "country": "us"
            },
            "contactMethod": null,
            "links": []
        },
    },
    expected: {
        response: {
            _id: '5fd7b950c912a333d0ee52bc',
            _objectType: 'contact',
            type: 'individual',
            name: {
                firstName: 'Test1234',
                lastName: 'Shah',
                fullName: 'Test1234 Shah'
            },
            groups: [],
            company: [],
            isGlobal: false,
            position: null,
            address: {
                addressLine: '390 Ontario Cir.',
                city: 'Boston',
                postalCode: '02130',
                state: 'Massachusetts',
                country: 'us'
            },
            contactMethod: null,
            links: []
        },
    }
};

var createObligation = {
    Request: {
        allParameters: {

            "_objectType": "contract",
            "attachment": [],
            "name": "Test-Operations and Maintenance Agreement",
            "referenceId": "referenceId:26262161",
            "active": true,
            "primaryContactPersons": null,
            "links": [],
            "area": "nerc",
            "description": "desc",
            "type": "contract",
            "responsibleParties": "Test company",
            "startDate": startDate,
            "endDate": endDate,
            "status": "compliant"
        },

    }
}
module.exports.testData = {
    createProject: createProject,
    createContact: createContact,
    createProjectGroup: createProjectGroup,
    createTask: createTask,
    createObligation: createObligation,
    updateTask: updateTask,
    retrieveTask: retrieveTask
};