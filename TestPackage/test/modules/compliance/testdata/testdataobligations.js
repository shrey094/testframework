var startDate = "01/04/2021";
var startDateResponse = "2021-01-04T00:00:00.000Z";
var endDate = "2021-11-25T00:00:00.000Z";
var endDateResponse = "2021-11-25T00:00:00.000Z";

var createObligation = {
    Request: {
        allParameters: {

            "_objectType": "contract",
            "attachment": [],
            "name": "Test-Operations and Maintenance Agreement",
            "referenceId": "referenceId:26262161",
            "active": true,
            "primaryContactPersons": [
                {
                    "_id": "5f68d493fcbc969763b5bf88",
                    "_objectType": "contact"
                }
            ],
            "links": [],
            "area": "nerc",
            "description": "desc",
            "type": "contract",
            "responsibleParties": "Test company",
            "startDate": startDate,
            "endDate": endDate,
            "status": "compliant"
        },

        mandatoryFields: {
            "_objectType": "contract",
            "name": "Test-Operations and Maintenance Agreement",
            "type": "contract",
            "startDate": startDate,
            "status": "compliant"
        },
        withProjectLink: {

            "_objectType": "contract",
            "attachment": [],
            "name": "Test-Operations and Maintenance Agreement",
            "referenceId": "referenceId:26262161",
            "active": true,
            "primaryContactPersons": [
                {
                    "_id": "5f68d493fcbc969763b5bf88",
                    "_objectType": "contact"
                }
            ],
            "links": [{
                "_objectType": "project",
                "_id": ""
            }],
            "area": "nerc",
            "description": "desc",
            "type": "contract",
            "responsibleParties": "Test company",
            "startDate": startDate,
            "endDate": endDate,
            "status": "compliant"

        },
        withProjectGroup: {

            "_objectType": "contract",
            "attachment": [],
            "name": "Test-Operations and Maintenance Agreement",
            "referenceId": "referenceId:26262161",
            "active": true,
            "primaryContactPersons": [
                {
                    "_id": "5f68d493fcbc969763b5bf88",
                    "_objectType": "contact"
                }
            ],
            "links": [
                {
                    "_objectType": "projectGroup",
                    "_id": ""
                }
            ],
            "area": "nerc",
            "description": "desc",
            "type": "contract",
            "responsibleParties": "Test company",
            "startDate": startDate,
            "endDate": endDate,
            "status": "compliant"

        },
        withoutLinks: {

            "_objectType": "contract",
            "attachment": [],
            "name": "Test-Operations and Maintenance Agreement",
            "referenceId": "referenceId:26262161",
            "active": true,
            "primaryContactPersons": [
                {
                    "_id": "5f68d493fcbc969763b5bf88",
                    "_objectType": "contact"
                }
            ],
            "links": [],
            "area": "nerc",
            "description": "desc",
            "type": "contract",
            "responsibleParties": "Test company",
            "startDate": startDate,
            "endDate": endDate,
            "status": "compliant"

        },
        inactiveObligation: {

            "_objectType": "contract",
            "attachment": [],
            "name": "Test-Operations and Maintenance Agreement",
            "referenceId": "referenceId:26262161",
            "active": false,
            "primaryContactPersons": [
                {
                    "_id": "5f68d493fcbc969763b5bf88",
                    "_objectType": "contact"
                }
            ],
            "links": [],
            "area": "nerc",
            "description": "desc",
            "type": "contract",
            "responsibleParties": "Test company",
            "startDate": startDate,
            "endDate": endDate,
            "status": "compliant"

        },
        createMultiple: [{
            "_objectType": "contract",
            "attachment": [],
            "name": "Test-Operations and Maintenance Agreement",
            "referenceId": "referenceId:26262161",
            "active": true,
            "primaryContactPersons": [
                {
                    "_id": "5f68d493fcbc969763b5bf88",
                    "_objectType": "contact"
                }
            ],
            "links": [
            ],
            "area": "nerc",
            "description": "desc",
            "type": "contract",
            "responsibleParties": "Test company",
            "startDate": startDate,
            "endDate": endDate,
            "status": "compliant"
        },
        {
            "_objectType": "contract",
            "attachment": [],
            "name": "Test-Operations and Maintenance Agreement",
            "referenceId": "referenceId:26262161",
            "active": true,
            "primaryContactPersons": [
                {
                    "_id": "5f68d493fcbc969763b5bf88",
                    "_objectType": "contact"
                }
            ],
            "links": [
            ],
            "area": "nerc",
            "description": "desc",
            "type": "contract",
            "responsibleParties": "Test company",
            "startDate": startDate,
            "endDate": endDate,
            "status": "compliant"
        }]
    },

    Response: {
        allParameters: {

            _id: '5ff34d88e51bab36783bce9c',
            _objectType: 'contract',
            attachment: [],
            name: 'Test-Operations and Maintenance Agreement',
            referenceId: 'referenceId:26262161',
            active: true,
            primaryContactPersons: [
                {
                    _id: '5f68d493fcbc969763b5bf88',
                    _objectType: 'contact',
                    name: 'sshah'
                }
            ],
            links: [],
            area: 'nerc',
            description: 'desc',
            type: 'contract',
            responsibleParties: 'Test company',
            startDate: startDateResponse,
            endDate: endDate,
            _count: { clause: 0, task: 0 },
            tasks: [],
            status: 'compliant',
            "source": null,
            "statusNotes": null

        },
        withProjectLinks: {
            "_id": "5fda2d5fd04ebe08844f670e",
            "_objectType": "contract",
            "attachment": [],
            "name": "Test-Operations and Maintenance Agreement",
            "referenceId": "referenceId:26262161",
            "active": true,
            "primaryContactPersons": [
                {
                    "_id": "5f68d493fcbc969763b5bf88",
                    "_objectType": "contact",
                    "name": "sshah"
                }
            ],
            links: [
                {
                    _id: '5fd8dffd3434aa11342d28b1',
                    _objectType: 'project',
                    name: 'test',
                    projectKey: 'projectcreate-kjb26',
                    _relatedTo: '5fd8e0033434aa11342d28cc',
                    _isDirect: true
                }
            ],
            "area": "nerc",
            "description": "desc",
            "type": "contract",
            "responsibleParties": "Test company",
            "startDate": startDateResponse,
            "endDate": endDateResponse,
            "_count": {
                "clause": 0,
                "task": 0
            },
            "tasks": [],
            "status": "compliant",
            "source": null,
            "statusNotes": null
        },

        mandatoryFields: {
            "_id": "5fda374ed04ebe08844f6718",
            "_objectType": "contract",
            "name": "Test-Operations and Maintenance Agreement",
            "type": "contract",
            "startDate": startDateResponse,
            "_count": {
                "clause": 0,
                "task": 0
            },
            "tasks": [],
            "status": "compliant",

            "source": null,
            "statusNotes": null,
            "referenceId": null,
            "primaryContactPersons": null,
            "active": true,
            "description": null,
            "endDate": null,
            "responsibleParties": null,
            "attachment": null,
            "links": null
        },
        withProjectGroup: {
            "_id": "5fe10973d85dc70f5470457c",
            "_objectType": "contract",
            "attachment": [

            ],

            "source": null,
            "statusNotes": null,
            "name": "Test-Operations and Maintenance Agreement",
            "referenceId": "referenceId:26262161",
            "active": true,
            "primaryContactPersons": [
                {
                    "_id": "5f68d493fcbc969763b5bf88",
                    "_objectType": "contact",
                    "name": "sshah"
                }
            ],
            "links": [
                {
                    "_id": "",
                    "_objectType": "projectGroup",
                    "projectGroupKey": "",
                    "name": "test1234d",
                    "_relatedTo": "",
                    "_isDirect": true
                }
            ],
            "area": "nerc",
            "description": "desc",
            "type": "contract",
            "responsibleParties": "Test company",
            "startDate": startDateResponse,
            "endDate": endDateResponse,
            "_count": {
                "clause": 0,
                "task": 0
            },
            "tasks": [

            ],
            "status": "compliant"
        },
        withoutLinks: {
            "_id": "5fda2d5fd04ebe08844f670e",
            "_objectType": "contract",
            "attachment": [],
            "name": "Test-Operations and Maintenance Agreement",
            "referenceId": "referenceId:26262161",
            "active": true,
            "primaryContactPersons": [
                {
                    "_id": "5f68d493fcbc969763b5bf88",
                    "_objectType": "contact",
                    "name": "sshah"
                }
            ],
            "links": [],
            "area": "nerc",
            "description": "desc",
            "type": "contract",
            "responsibleParties": "Test company",
            "startDate": startDateResponse,
            "endDate": endDateResponse,
            "_count": {
                "clause": 0,
                "task": 0
            },
            "tasks": [],
            "status": "compliant",
            "source": null,
            "statusNotes": null
        },
        inactiveObligation: {
            "_id": "5fda2d5fd04ebe08844f670e",
            "_objectType": "contract",
            "attachment": [],
            "name": "Test-Operations and Maintenance Agreement",
            "referenceId": "referenceId:26262161",
            "active": false,
            "primaryContactPersons": [
                {
                    "_id": "5f68d493fcbc969763b5bf88",
                    "_objectType": "contact",
                    "name": "sshah"
                }
            ],
            "links": [],
            "area": "nerc",
            "description": "desc",
            "type": "contract",
            "responsibleParties": "Test company",
            "startDate": startDateResponse,
            "endDate": endDateResponse,
            "_count": {
                "clause": 0,
                "task": 0
            },
            "tasks": [],
            "status": "compliant",
            "source": null,
            "statusNotes": null
        },
        createMultiple: [
            {
                "_objectType": "contract",
                "attachment": [],
                "name": "Test-Operations and Maintenance Agreement",
                "referenceId": "referenceId:26262161",
                "active": true,
                "primaryContactPersons": [
                    {
                        "_id": "5f68d493fcbc969763b5bf88",
                        "_objectType": "contact",
                        "name": "sshah"
                    }
                ],
                "links": [],
                "area": "nerc",
                "description": "desc",
                "type": "contract",
                "responsibleParties": "Test company",
                "startDate": startDateResponse,
                "endDate": endDateResponse,
                "_id": "5fe2021cde078a10e8d67dca",
                "_count": {
                    "clause": 0,
                    "task": 0
                },
                "tasks": [],
                "source": null,
                "statusNotes": null,
                "status": "compliant"
            },
            {
                "_objectType": "contract",
                "attachment": [],
                "name": "Test-Operations and Maintenance Agreement",
                "referenceId": "referenceId:26262161",
                "active": true,
                "primaryContactPersons": [
                    {
                        "_id": "5f68d493fcbc969763b5bf88",
                        "_objectType": "contact",
                        "name": "sshah"
                    }
                ],
                "links": [],
                "area": "nerc",
                "description": "desc",
                "type": "contract",
                "responsibleParties": "Test company",
                "startDate": startDateResponse,
                "endDate": endDateResponse,
                "_id": "5fe2021cde078a10e8d67dcb",
                "_count": {
                    "clause": 0,
                    "task": 0
                },
                "tasks": [],
                "source": null,
                "statusNotes": null,
                "status": "compliant"
            }
        ]
    }
};

var retrieveObligation = {
    request: {
        filterByObjectType: {
            "query": {
                "_objectType": "contract"
            }
        },
        filterByProject: {
            "query": {
                "_objectType": "contract",
                "links": [
                    {
                        "_objectType": "project",
                        "name": "Nitzsche, Hansen and Harris",
                        "projectKey": "project-x",
                        "_relatedTo": "5fe243eb182e86396844f142",
                        "_isDirect": true
                    }
                ]
            }
        }
    },
    response: {
        createOne: {
            _id: '5fe23b51182e86396844efac',
            _objectType: 'contract',
            attachment: [],
            name: 'Test-Operations and Maintenance Agreement',
            referenceId: 'referenceId:26262161',
            active: true,
            primaryContactPersons: [
                {
                    _id: '5f68d493fcbc969763b5bf88',
                    _objectType: 'contact',
                    name: 'sshah'
                }
            ],
            links: [],
            area: 'nerc',
            description: 'desc',
            type: 'contract',
            responsibleParties: 'Test company',
            startDate: startDateResponse,
            endDate: endDate,
            _count: { clause: 0, task: 0 },
            tasks: [],
            status: 'compliant',
            "source": null,
            "statusNotes": null
        }
    }
};

var updateObligation = {
    Request: {
        UpdateName_referenceId_description_responsibleParties: {
            "_id": "5fbe8fb7c82a6e046414ea75",
            "_objectType": "contract",
            "attachment": [],
            "name": "test",
            "referenceId": "1234",
            "active": true,
            "primaryContactPersons": [
                {
                    "_id": "5f68d493fcbc969763b5bf88",
                    "_objectType": "contact"
                }
            ],
            "links": [
                {
                    "_objectType": "project",
                    "_id": "5ed7df491896bfd3b3f8565d"
                }
            ],
            "description": "description",
            "type": "contract",
            "responsibleParties": "test",
            "startDate": startDate,
            "endDate": endDate
        },
        Update_PrimaryContact_type: {
            "_id": "5ff3554fe51bab36783bd2b6",
            "_objectType": "contract",
            "attachment": [],
            "name": "Test-Operations and Maintenance Agreement",
            "referenceId": "referenceId:26262161",
            "active": true,
            "primaryContactPersons": [
                {
                    "_id": "5f68d493fcbc969763b5bf88",
                    "_objectType": "contact"
                }
            ],
            "links": [],
            "area": "nerc",
            "description": "desc",
            "type": "contract",
            "responsibleParties": "Test company",
            "startDate": startDate,
            "endDate": endDate
        },

        Update_project_projectGroup: {
            "_id": "",
            "_objectType": "contract",
            "name": "Test-Operations and Maintenance Agreement",
            "type": "contract",
            "links": [
                {
                    "_objectType": "project",
                    "_id": "5fc828509b652831681ad584"
                },
                {
                    "_objectType": "projectGroup",
                    "_id": ""
                }
            ],
            "startDate": startDate
        },
        Update_dates: {
            "_id": "5ff3554fe51bab36783bd2b6",
            "_objectType": "contract",
            "attachment": [],
            "name": "Test-Operations and Maintenance Agreement",
            "referenceId": "referenceId:26262161",
            "active": true,
            "primaryContactPersons": null,
            "links": [],
            "area": "nerc",
            "description": "desc",
            "type": "contract",
            "responsibleParties": "Test company",
            "startDate": startDate,
            "endDate": "11/25/2022"
        },
        Update_dates_area: {
            "_id": "5ff3554fe51bab36783bd2b6",
            "_objectType": "contract",
            "attachment": [],
            "name": "Test-Operations and Maintenance Agreement",
            "referenceId": "referenceId:26262161",
            "active": true,
            "primaryContactPersons": null,
            "links": [],
            "area": "others",
            "description": "desc",
            "type": "contract",
            "responsibleParties": "Test company",
            "startDate": startDate,
            "endDate": "11/28/2022"
        }
    },
    Response: {
        UpdateName_referenceId_description_responsibleParties: {
            _id: '5ff49e0e072d493500504565',
            _objectType: 'contract',
            attachment: [],
            name: 'test',
            referenceId: '1234',
            active: true,
            primaryContactPersons: [
                {
                    _id: '5f68d493fcbc969763b5bf88',
                    _objectType: 'contact',
                    name: 'sshah'
                }
            ],
            links: [],
            "source": null,
            "statusNotes": null,

            description: 'description',
            type: 'contract',
            responsibleParties: 'test',
            startDate: startDateResponse,
            endDate: endDate,
            _count: { clause: 0, task: 0 },
            tasks: [],
            status: 'compliant',
            displayText: 'test'
        },
        Update_PrimaryContact_type: {
            "_id": "5ff3554fe51bab36783bd2b6",
            "_objectType": "contract",
            "attachment": [],
            "name": "Test-Operations and Maintenance Agreement",
            "referenceId": "referenceId:26262161",
            "active": true,
            "primaryContactPersons": [
                {
                    "_id": "5f68d493fcbc969763b5bf88",
                    "_objectType": "contact",
                    "name": "sshah"
                }
            ],
            "links": [],
            "area": "nerc",
            "description": "desc",
            "type": "contract",
            "responsibleParties": "Test company",
            "startDate": startDateResponse,
            "endDate": endDateResponse,
            "_count": {
                "clause": 0,
                "task": 0
            },
            "tasks": [],
            "status": "compliant",
            "displayText": "Test-Operations and Maintenance Agreement",
            "source": null,
            "statusNotes": null
        },
        Update_project_projectGroup: {
            _id: '5ff4bee23ff4a82e8cc64120',
            _objectType: 'contract',
            name: 'Test-Operations and Maintenance Agreement',
            type: 'contract',
            startDate: startDateResponse,
            _count: { clause: 0, task: 0 },
            tasks: [],
            status: 'compliant',

            referenceId: null,
            primaryContactPersons: [],
            active: true,
            description: null,
            endDate: null,
            responsibleParties: null,
            attachment: [],
            links: [
                {
                    _id: '5ff4bedb3ff4a82e8cc64113',
                    _objectType: 'project',
                    name: 'test',
                    projectKey: 'projectcreate-fv28s',
                    _relatedTo: '5ff4bee23ff4a82e8cc64120'
                },
                {
                    "_id": "5ffc4bba068d0f2fc07f64dc",
                    "_objectType": "projectGroup",
                    "_relatedTo": "5ffc4bb7068d0f2fc07f64a3",
                    "name": "test1234d",
                    "projectGroupKey": "project_group_create-kjkka"
                }
            ],
            displayText: 'Test-Operations and Maintenance Agreement',
            "source": null,
            "statusNotes": null
        },
        Update_dates: {
            "_id": "5ff3554fe51bab36783bd2b6",
            "_objectType": "contract",
            "attachment": [],
            "name": "Test-Operations and Maintenance Agreement",
            "referenceId": "referenceId:26262161",
            "active": true,
            "primaryContactPersons": [],
            "links": [],
            "area": "nerc",
            "description": "desc",
            "type": "contract",
            "responsibleParties": "Test company",
            "startDate": startDateResponse,
            "endDate": "2022-11-25T00:00:00.000Z",
            "_count": {
                "clause": 0,
                "task": 0
            },
            "tasks": [],
            "status": "compliant",
            "displayText": "Test-Operations and Maintenance Agreement",
            "source": null,
            "statusNotes": null
        },
        Update_dates_area: {
            _id: '5ff898ff6455b61f88bdd1a0',
            _objectType: 'contract',
            name: 'Test-Operations and Maintenance Agreement',
            type: 'contract',
            "startDate": "2021-01-04T00:00:00.000Z",
            "endDate": "2022-11-28T00:00:00.000Z",
            _count: { clause: 0, task: 0 },
            tasks: [],
            status: 'compliant',
            area: 'others',
            referenceId: 'referenceId:26262161',
            primaryContactPersons: [],
            active: true,
            description: 'desc',
            responsibleParties: 'Test company',
            attachment: [],
            links: [],
            displayText: 'Test-Operations and Maintenance Agreement',
            "source": null,
            "statusNotes": null
        }
    }
};

var createProject = {
    input: {
        "_objectType": "project",
        "name": "test",
        "projectKey": "test",
        "type": "biomass",
        "size": {
            "value": null,
            "unit": "kW"
        },
        "startDate": startDate,
        "status": null,
        "description": null,
        "address": {
            "addressLine": null,
            "city": null,
            "postalCode": null,
            "state": null,
            "country": null
        },
        "geoLocation": {
            "type": "Point",
            "coordinates": [

            ]
        },
        "siteInfo": {
            "siteAccess": {
                "lessor": [

                ]
            },
            "siteOwnership": {
                "owner": [

                ]
            }
        },
        "businessDays": [
            "monday",
            "tuesday",
            "wednesday",
            "thursday",
            "friday"
        ],
        "links": [

        ]
    },
    expected: {
        "_objectType": "project",
        "projectKey": "test",
        "name": "Test Project Create",
        "description": "Create Works!",
        "type": "solar",
        "startDate": startDateResponse,
        "address": {
            "addressLine": "390 Ontario Cir.",
            "city": "Boston",
            "postalCode": "02130",
            "state": "Massachusetts",
            "country": "US"
        },
        "geoLocation": {
            "type": "Point",
            "coordinates": [1, 2]
        },
        "size": {
            "value": 2500,
            "unit": "kW"
        },
        "keyContact": [{
            "_objectType": "contact",
            "_id": "5ed918121896bfd3b3f85725",
            "name": "Linda Leader"
        }],
        "owner": [{
            "_objectType": "contact",
            "_id": "5ed9195c1896bfd3b3f8573b",
            "name": "Solra Power, Inc."
        }],
        "omOperator": [{
            "_objectType": "contact",
            "_id": "5ed91a4c1896bfd3b3f8574d",
            "name": "Warm Sun O&M"
        }],
        "interconnection": [{
            "_objectType": "contact",
            "_id": "5ed91a581896bfd3b3f8574e",
            "name": "Summer InterConnectors"
        }],
        "utility": [{
            "_objectType": "contact",
            "_id": "5ed91a661896bfd3b3f8574f",
            "name": "Eversource"
        }],
        "offtaker": [{
            "_objectType": "contact",
            "_id": "5ed91a741896bfd3b3f85750",
            "name": "Tradeenergy America"
        }],
        "status": "operational",
        "businessDays": ["monday", "tuesday"],
        "financialYearEnd": 12,
        "attachment": [],
        "timezone": {
            "region": null,
            "city": null,
            "databaseName": null
        },
        "siteInfo": {
            "siteAccess": {
                "instructions": "Access to the site is through the entrance located at South-East corner. Gate Access Code: 94413",
                "lessor": [{
                    "_id": "5ed9195c1896bfd3b3f8573b",
                    "_objectType": "contact",
                    "name": "Solra Power, Inc."
                }],
                "leaseTermMonth": 60,
                "leaseStartDate": "2016-04-01T04:00:00.000Z",
                "leaseEndDate": "2021-03-31T04:00:00.000Z"
            },
            "siteOwnership": {
                "owner": [{
                    "_id": "5ed9195c1896bfd3b3f8573b",
                    "_objectType": "contact",
                    "name": "Solra Power, Inc."
                }],
                "securityGateCode": "94413"
            }
        },
        "links": []
    }
};

var createProjectGroup = {
    Request: {
        allParameters: {
            "_objectType": "projectGroup",
            "projectGroupKey": "test1234d",
            "name": "test1234d"
        }
    },
    Response: {
        allParameters: {
            "_id": "5fe0f59ad85dc70f547044cc",
            "_objectType": "projectGroup",
            "projectGroupKey": "test1234d",
            "name": "test1234d",
            "_count": {},
            "links": []
        }
    }
}

var createContact = {

    validRequest: {

        allParameters: {
            "_objectType": "contact",
            "type": "individual",
            "name": {
                "firstName": "Test1234",
                "lastName": "Shah",
                "fullName": "Test1234 Shah"
            },
            "groups": [],
            "company": [],
            "isGlobal": false,
            "position": null,
            "address": {
                "addressLine": "390 Ontario Cir.",
                "city": "Boston",
                "postalCode": "02130",
                "state": "Massachusetts",
                "country": "us"
            },
            "contactMethod": null,
            "links": []
        },
    },
    expected: {
        response: {
            _id: '5fd7b950c912a333d0ee52bc',
            _objectType: 'contact',
            type: 'individual',
            name: {
                firstName: 'Test1234',
                lastName: 'Shah',
                fullName: 'Test1234 Shah'
            },
            groups: [],
            company: [],
            isGlobal: false,
            position: null,
            address: {
                addressLine: '390 Ontario Cir.',
                city: 'Boston',
                postalCode: '02130',
                state: 'Massachusetts',
                country: 'us'
            },
            contactMethod: null,
            links: []
        },
    }
};

module.exports.testData = {
    createObligation: createObligation,
    createProject: createProject,
    createContact: createContact,
    createProjectGroup: createProjectGroup,
    retrieveObligation: retrieveObligation,
    updateObligation: updateObligation

};