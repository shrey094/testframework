process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0;
const { axios, assert, ObjectId } = require('../../../lib/mochalibs').mochalibs;
const { server, clientKey } = require('../../../lib/dbconfig').dbconfig;
const { createObligation, createProject, createProjectGroup } = require('../testdata/testdataobligations').testData;
const { removePropFromJSON, deepEqual } = require('../../../utils/mochautils').mochautils;
const { deleteOne } = require('../../../utils/dbwrapper').dbwrapper;
var id, project, projectId;


describe('Obligation create tests', async function () {

    beforeEach('create project', async function () {
        let response = {};
        var projectKey = "projectcreate-" + Math.random().toString(36).substring(2, 7);
        createProject.input.projectKey = projectKey;
        try {
            debugger;
            response = await axios.post(`${server}/${clientKey}/package/powerhub/projectportfolio/api/create/project.js`, createProject.input);
            projectId = response.data._id
            project = await removePropFromJSON(response.data, ["_vendorKey", "_clientKey", "_ownerKey", "_dateTimeCreated", "_createdBy", "_dateTimeModified", "_modifiedBy", "_signature"])
        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;
           
        }
        assert(response.status == 200, `Request to create project with all the fields is failing: ${response.status} `);
    });

    afterEach('delete obligations', async function () {
        await deleteOne("compliance", { "_objectType": "contract", "_id": ObjectId(id) });
    });

    after('delete project', async function () {
        await deleteOne("projects", { "_objectType": "project", "_id": ObjectId(projectId) });
    });



    it('1. create obligation', async function () {
        let response = {};
        try {
            response = await axios.post(`${server}/${clientKey}/package/powerhub/compliance/api/create/contract.js`, createObligation.Request.allParameters);
            id = response.data._id;
            var actual = await removePropFromJSON(response.data, ["_vendorKey", "_clientKey", "_ownerKey", "_dateTimeCreated", "_createdBy", "_dateTimeModified", "_modifiedBy", "_signature"])
        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;
        }
        //update values in expected response to match with actual response
        createObligation.Response.allParameters["_id"] = id;
        // Verify if the test was successful.
        assert(response.status == 200, `Request to create obligation with all the fields is failing: ${response.status}`);
        deepEqual(actual, createObligation.Response.allParameters); //Verify the whole object
    });

    it('2. create obligation with only mandatory fields', async function () {
        let response = {};
        try {
            response = await axios.post(`${server}/${clientKey}/package/powerhub/compliance/api/create/contract.js`, createObligation.Request.mandatoryFields);
            id = response.data._id;
            var actual = await removePropFromJSON(response.data, ["_vendorKey", "_clientKey", "_ownerKey", "_dateTimeCreated", "_createdBy", "_dateTimeModified", "_modifiedBy", "_signature"])
        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;
        }
        //update values in expected response to match with actual response
        createObligation.Response.mandatoryFields["_id"] = id;
        // Verify if the test was successful.
        assert(response.status == 200, `create obligation with only mandatory fields is failing: ${response.status} `);
        deepEqual(actual, createObligation.Response.mandatoryFields); //Verify the whole object 
    });

    it('3. Create Obligation and assign to Project', async function () {
        let response = {};
        try {
            //Update new ids for links in request body before sending request
            createObligation.Request.withProjectLink.links[0]._id = projectId
            response = await axios.post(`${server}/${clientKey}/package/powerhub/compliance/api/create/contract.js`, createObligation.Request.withProjectLink);
            id = response.data._id;
            var actual = await removePropFromJSON(response.data, ["_vendorKey", "_clientKey", "_ownerKey", "_dateTimeCreated", "_createdBy", "_dateTimeModified", "_modifiedBy", "_signature"])
        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;
        }
        //update values in expected response to match with actual response
        createObligation.Response.withProjectLinks["_id"] = id;
        createObligation.Response.withProjectLinks.links[0]._relatedTo = id;
        createObligation.Response.withProjectLinks.links[0]._id = projectId;
        createObligation.Response.withProjectLinks.links[0].projectKey = project.projectKey;
        // Verify if the test was successful.
        assert(response.status == 200, `Create Obligation and assign to Project is failing : ${response.status} `);
        deepEqual(actual, createObligation.Response.withProjectLinks); //Verify the whole object});
    });

    it('4. Create Obligation and assign to Project group', async function () {
        let response = {};
        let projectGroupRequest = {};
        try {
            // Create a project group
            var projectGroupKey = "project_group_create-" + Math.random().toString(36).substring(2, 7);
            createProjectGroup.Request.allParameters.projectGroupKey = projectGroupKey;
            projectGroupRequest = await axios.post(`${server}/${clientKey}/package/powerhub/projectportfolio/api/create/group.js`, createProjectGroup.Request.allParameters);
            var projectGroup = await removePropFromJSON(projectGroupRequest.data, ["_vendorKey", "_clientKey", "_ownerKey", "_dateTimeCreated", "_createdBy", "_dateTimeModified", "_modifiedBy", "_signature"])
            var projectGroupId = projectGroup._id;

            //Create an obligation and assign to project group
            createObligation.Request.withProjectGroup.links[0]._id = projectGroupId;
            response = await axios.post(`${server}/${clientKey}/package/powerhub/compliance/api/create/contract.js`, createObligation.Request.withProjectGroup);
            id = response.data._id;
            var actual = await removePropFromJSON(response.data, ["_vendorKey", "_clientKey", "_ownerKey", "_dateTimeCreated", "_createdBy", "_dateTimeModified", "_modifiedBy", "_signature"])
        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;
        }
        //update values in expected response to match with actual response
        createObligation.Response.withProjectGroup["_id"] = id;
        createObligation.Response.withProjectGroup.links[0]._id = projectGroup._id;
        createObligation.Response.withProjectGroup.links[0]._relatedTo = id;
        createObligation.Response.withProjectGroup.links[0].projectGroupKey = projectGroup.projectGroupKey;
        // Verify if the test was successful.
        assert(response.status == 200, `Request to Create Obligation and assign to Project group is failing : ${response.status} `);
        deepEqual(actual, createObligation.Response.withProjectGroup); //Verify the whole object});
    });

    it('5. Create Obligation and assign to none', async function () {
        let response = {};
        try {
            response = await axios.post(`${server}/${clientKey}/package/powerhub/compliance/api/create/contract.js`, createObligation.Request.withoutLinks);
            id = response.data._id;
            var actual = await removePropFromJSON(response.data, ["_vendorKey", "_clientKey", "_ownerKey", "_dateTimeCreated", "_createdBy", "_dateTimeModified", "_modifiedBy", "_signature"])
        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;
        }
        //update values in expected response to match with actual response
        createObligation.Response.withoutLinks["_id"] = id;
        // Verify if the test was successful.
        assert(response.status == 200, `Request to create contact with all the fields is failing : ${response.status} `);
        deepEqual(actual, createObligation.Response.withoutLinks); //Verify the whole object });
    });

    it('6. Create inactive obligation', async function () {
        let response = {};
        try {
            response = await axios.post(`${server}/${clientKey}/package/powerhub/compliance/api/create/contract.js`, createObligation.Request.inactiveObligation);
            id = response.data._id;
            var actual = await removePropFromJSON(response.data, ["_vendorKey", "_clientKey", "_ownerKey", "_dateTimeCreated", "_createdBy", "_dateTimeModified", "_modifiedBy", "_signature"])
        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;
        }
        //update values in expected response to match with actual response
        createObligation.Response.inactiveObligation["_id"] = id;
        // Verify if the test was successful.
        assert(response.status == 200, `Request to create contact with all the fields is failing: ${response.status} `);
        deepEqual(actual, createObligation.Response.inactiveObligation); //Verify the whole object});
    });

    it('7. Create multiple', async function () {
        let response = {};
        let id2;
        try {
            response = await axios.post(`${server}/${clientKey}/package/powerhub/compliance/api/create/contracts.js`, createObligation.Request.createMultiple);
            id = response.data[0]._id;
            id2 = response.data[1]._id;
        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;
        }
        createObligation.Response.createMultiple[0]._id = id;
        createObligation.Response.createMultiple[1]._id = id2;
        assert(response.status == 200, `Request to create contact with all the fields is failing : ${response.status} `);
        deepEqual(response.data, createObligation.Response.createMultiple);
    });
});