process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0;
const { axios, assert, ObjectId } = require('../../../lib/mochalibs').mochalibs;
const { server, clientKey } = require('../../../lib/dbconfig').dbconfig;
const { createObligation, retrieveObligation } = require('../testdata/testdataobligations').testData;
const { removePropFromJSON, deepEqual } = require('../../../utils/mochautils').mochautils;
const { deleteOne } = require('../../../utils/dbwrapper').dbwrapper;
var id;


describe('Obligation retrieve tests', async function () {

    beforeEach('create obligation', async function () {
        let response = {};
        try {
            response = await axios.post(`${server}/${clientKey}/package/powerhub/compliance/api/create/contract.js`, createObligation.Request.allParameters);
            id = response.data._id;
            var actual = await removePropFromJSON(response.data, ["_vendorKey", "_clientKey", "_ownerKey", "_dateTimeCreated", "_createdBy", "_dateTimeModified", "_modifiedBy", "_signature"])
        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;
        }
        //update values in expected response to match with actual response
        createObligation.Response.allParameters["_id"] = id;
        // Verify if the test was successful.
        assert(response.status == 200, `Request to create obligation with all the fields is failing : ${response.status} `);
        deepEqual(actual, createObligation.Response.allParameters); //Verify the whole object

    });

    afterEach('delete obligations', async function () {
        await deleteOne("compliance", { "_objectType": "contract", "_id": ObjectId(id) });
    });


    it('1. retrieve obligation by ID', async function () {
        let response = {};
        try {
            response = await axios.post(`${server}/${clientKey}/package/powerhub/compliance/api/retrieve/contract.js`, { "_id": id });
            id = response.data._id;
            var actual = await removePropFromJSON(response.data, ["_vendorKey", "_clientKey", "_ownerKey", "_dateTimeCreated", "_createdBy", "_dateTimeModified", "_modifiedBy", "_signature"])
        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;
        }
        retrieveObligation.response.createOne["_id"] = id;
        // Verify if the test was successful.
        assert(response.status == 200, `Request to retrieve obligation by id with all the fields is failing : ${response.status} `);
        deepEqual(actual, retrieveObligation.response.createOne); //Verify the whole object
    });

    it('2. retrieve multiple obligations by objectType', async function () {
        let response = {};
        try {
            response = await axios.post(`${server}/${clientKey}/package/powerhub/compliance/api/retrieve/contracts.js`, retrieveObligation.request.filterByObjectType);
        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;
        }
        assert(response.status == 200, `Request to retrieve multiple obligations  by objectType with all the fields is failing : ${response.status} `);
    });


});