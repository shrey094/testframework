process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0;
const { axios, assert, ObjectId } = require('../../../lib/mochalibs').mochalibs;
const { server, clientKey } = require('../../../lib/dbconfig').dbconfig;
const { createObligation } = require('../testdata/testdataobligations').testData;
const { removePropFromJSON, deepEqual } = require('../../../utils/mochautils').mochautils;
const { deleteOne } = require('../../../utils/dbwrapper').dbwrapper;
var id;


describe('Obligation Delete tests', async function () {

    beforeEach('create obligation', async function () {
        let response = {};
        try {
            response = await axios.post(`${server}/${clientKey}/package/powerhub/compliance/api/create/contract.js`, createObligation.Request.allParameters);
            id = response.data._id;
            var actual = await removePropFromJSON(response.data, ["_vendorKey", "_clientKey", "_ownerKey", "_dateTimeCreated", "_createdBy", "_dateTimeModified", "_modifiedBy", "_signature"])
        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;

        }
        //update values in expected response to match with actual response
        createObligation.Response.allParameters["_id"] = id;

        // Verify if the test was successful.
        assert(response.status == 200, "Request to create obligation with all the fields is failing");
        deepEqual(actual, createObligation.Response.allParameters); //Verify the whole object

    });

    afterEach('delete obligations', async function () {
        await deleteOne("compliance", { "_objectType": "contract", "_id": ObjectId(id) });
    });


    it('1. delete obligation', async function () {
        let response = {};
        try {
            response = await axios.post(`${server}/${clientKey}/package/powerhub/compliance/api/delete/contract.js`, { "_id": id });
        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;
        }
        // Verify if the test was successful.
        assert(response.status == 200, `Request to delete obligation with all the fields is failing : ${response.status} `);
        deepEqual(response.data, { "deleted": 1 }); //Verify the whole object
    });


});