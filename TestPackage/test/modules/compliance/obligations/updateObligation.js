process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0;
const { axios, assert, ObjectId } = require('../../../lib/mochalibs').mochalibs;
const { server, clientKey } = require('../../../lib/dbconfig').dbconfig;
const { createObligation, updateObligation, createProject, createContact, createProjectGroup } = require('../testdata/testdataobligations').testData;
const { removePropFromJSON, deepEqual } = require('../../../utils/mochautils').mochautils;
const { deleteOne } = require('../../../utils/dbwrapper').dbwrapper;
var id, project, projectId, contactId;


describe('Obligation update tests', async function () {

    beforeEach('create obligation', async function () {
        let response = {};
        try {

            response = await axios.post(`${server}/${clientKey}/package/powerhub/compliance/api/create/contract.js`, createObligation.Request.mandatoryFields);
            id = response.data._id;
            var actual = await removePropFromJSON(response.data, ["_vendorKey", "_clientKey", "_ownerKey", "_dateTimeCreated", "_createdBy", "_dateTimeModified", "_modifiedBy", "_signature"])
        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;
        }
        //update values in expected response to match with actual response
        createObligation.Response.mandatoryFields["_id"] = id;
        // Verify if the test was successful.
        assert(response.status == 200, `Request to create obligation with all the fields is failing : ${response.status} `);
        deepEqual(actual, createObligation.Response.mandatoryFields); //Verify the whole object

    });

    before('create a Project', async function () {
        let response = {};
        var projectKey = "projectcreate-" + Math.random().toString(36).substring(2, 7);
        createProject.input.projectKey = projectKey;
        try {
            response = await axios.post(`${server}/${clientKey}/package/powerhub/projectportfolio/api/create/project.js`, createProject.input);
            projectId = response.data._id
            project = await removePropFromJSON(response.data, ["_vendorKey", "_clientKey", "_ownerKey", "_dateTimeCreated", "_createdBy", "_dateTimeModified", "_modifiedBy", "_signature"])
        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;
        }
        assert(response.status == 200, `Request to create project with all the fields is failing: ${response.status} `);
    });

    afterEach('delete obligations', async function () {
        await deleteOne("compliance", { "_objectType": "contract", "_id": ObjectId(id) });
    });

    after('delete project, contact', async function () {
        await deleteOne("contacts", { "_objectType": "contact", "_id": ObjectId(contactId) });
        await deleteOne("projects", { "_objectType": "project", "_id": ObjectId(projectId) });
    });


    it('1. Update obligation fields: Name, reference id, description, responsible parties', async function () {
        let response = {};
        try {
            updateObligation.Request.UpdateName_referenceId_description_responsibleParties._id = id;
            response = await axios.post(`${server} /${clientKey}/package/powerhub/compliance/api/update/contract.js`, updateObligation.Request.UpdateName_referenceId_description_responsibleParties);
            id = response.data._id;
            var actual = await removePropFromJSON(response.data, ["_vendorKey", "_clientKey", "_ownerKey", "_dateTimeCreated", "_createdBy", "_dateTimeModified", "_modifiedBy", "_signature"])
        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;
        }
        //update values in expected response to match with actual response
        updateObligation.Response.UpdateName_referenceId_description_responsibleParties["_id"] = id;

        // Verify if the test was successful.
        assert(response.status == 200, `Request to create obligation with all the fields is failing : ${response.status} `);
        deepEqual(actual, updateObligation.Response.UpdateName_referenceId_description_responsibleParties); //Verify the whole object
    });

    //FIXME: 
    it('2. Update: Primary contact, type', async function () {
        let response = {};
        let contact = {};
        try {
            //create a contact
            contact = await axios.post(`${server} /${clientKey}/package/powerhub/contacts/api/create/contact.js`, createContact.validRequest.allParameters);
            contactId = contact.data._id;

            //update obligation
            updateObligation.Request.Update_PrimaryContact_type._id = id;
            updateObligation.Request.Update_PrimaryContact_type.primaryContactPersons[0]._id = contactId;
            response = await axios.post(`${server} /${clientKey}/package/powerhub/compliance/api/update/contract.js`, updateObligation.Request.Update_PrimaryContact_type);
            id = response.data._id;
            var actual = await removePropFromJSON(response.data, ["_vendorKey", "_clientKey", "_ownerKey", "_dateTimeCreated", "_createdBy", "_dateTimeModified", "_modifiedBy", "_signature"])
        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;
        }
        //update values in expected response to match with actual response
        updateObligation.Response.Update_PrimaryContact_type["_id"] = id;
        updateObligation.Response.Update_PrimaryContact_type.primaryContactPersons[0]._id = contactId;

        // Verify if the test was successful.
        assert(response.status == 200, `Request to create obligation with all the fields is failing : ${response.status} `);
        deepEqual(actual, updateObligation.Response.Update_PrimaryContact_type); //Verify the whole object
    });

    it('3. Update assign to project', async function () {
        let response = {};
        let projectGroupRequest = {};
        try {
            // Create a project group
            var projectGroupKey = "project_group_create-" + Math.random().toString(36).substring(2, 7);
            createProjectGroup.Request.allParameters.projectGroupKey = projectGroupKey;
            projectGroupRequest = await axios.post(`${server} /${clientKey}/package/powerhub/projectportfolio/api/create/group.js`, createProjectGroup.Request.allParameters);
            var projectGroup = await removePropFromJSON(projectGroupRequest.data, ["_vendorKey", "_clientKey", "_ownerKey", "_dateTimeCreated", "_createdBy", "_dateTimeModified", "_modifiedBy", "_signature"])
            var projectGroupId = projectGroup._id;
            //Update new ids for links in request body before sending request
            updateObligation.Request.Update_project_projectGroup._id = id;
            updateObligation.Request.Update_project_projectGroup.links[0]._id = projectId;
            updateObligation.Request.Update_project_projectGroup.links[1]._id = projectGroupId;
            response = await axios.post(`${server} /${clientKey}/package/powerhub/compliance/api/update/contract.js`, updateObligation.Request.Update_project_projectGroup);
            id = response.data._id;
            var actual = await removePropFromJSON(response.data, ["_vendorKey", "_clientKey", "_ownerKey", "_dateTimeCreated", "_createdBy", "_dateTimeModified", "_modifiedBy", "_signature"])
        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;
        }
        //update values in expected response to match with actual response
        updateObligation.Response.Update_project_projectGroup["_id"] = id;
        updateObligation.Response.Update_project_projectGroup.links[0]._relatedTo = id;
        updateObligation.Response.Update_project_projectGroup.links[0]._id = projectId;
        updateObligation.Response.Update_project_projectGroup.links[0].projectKey = project.projectKey;
        updateObligation.Response.Update_project_projectGroup.links[1]._relatedTo = id;
        updateObligation.Response.Update_project_projectGroup.links[1]._id = projectGroupId;
        updateObligation.Response.Update_project_projectGroup.links[1].projectGroupKey = projectGroup.projectGroupKey;
        // Verify if the test was successful.
        assert(response.status == 200, `3. Request to Update assign to project to Assign to project groups is failing : ${response.status} `);
        deepEqual(actual, updateObligation.Response.Update_project_projectGroup); //Verify the whole object
    });

    it('4. Update Start date and end date', async function () {
        let response = {};
        try {
            //Update new ids for links in request body before sending request
            updateObligation.Request.Update_dates._id = id;
            response = await axios.post(`${server} /${clientKey}/package/powerhub/compliance/api/update/contract.js`, updateObligation.Request.Update_dates);
            id = response.data._id;
            var actual = await removePropFromJSON(response.data, ["_vendorKey", "_clientKey", "_ownerKey", "_dateTimeCreated", "_createdBy", "_dateTimeModified", "_modifiedBy", "_signature"])
        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;
        }
        //update values in expected response to match with actual response
        updateObligation.Response.Update_dates["_id"] = id;

        // Verify if the test was successful.
        assert(response.status == 200, `Request to create obligation with all the fields is failing : ${response.status} `);
        deepEqual(actual, updateObligation.Response.Update_dates); //Verify the whole object

    });

    it('5.Update Many: Area and dates', async function () {
        let response = {};
        try {
            //Update new ids for links in request body before sending request
            updateObligation.Request.Update_dates_area._id = id;
            response = await axios.post(`${server} /${clientKey}/package/powerhub/compliance/api/update/contract.js`, updateObligation.Request.Update_dates_area);
            id = response.data._id;
            var actual = await removePropFromJSON(response.data, ["_vendorKey", "_clientKey", "_ownerKey", "_dateTimeCreated", "_createdBy", "_dateTimeModified", "_modifiedBy", "_signature"])
        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;
        }
        //update values in expected response to match with actual response
        updateObligation.Response.Update_dates_area["_id"] = id;

        // Verify if the test was successful.
        assert(response.status == 200, `Request to create obligation with all the fields is failing : ${response.status} `);
        deepEqual(actual, updateObligation.Response.Update_dates_area); //Verify the whole object
    });

    //TODO:
    it('Remove added attachment', async function () {

    });

    //TODO:
    it('Update 1 more attachments', async function () {

    });



});