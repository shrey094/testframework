process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0;
const { axios, assert, ObjectId } = require('../../../lib/mochalibs').mochalibs;
const { server, clientKey } = require('../../../lib/dbconfig').dbconfig;
const { createObligation, createContact, createTask, updateTask } = require('../testdata/test_data_tasks').testData;
const { removePropFromJSON, deepEqual } = require('../../../utils/mochautils').mochautils;
const { deleteOne } = require('../../../utils/dbwrapper').dbwrapper;
var id, taskId, contactId, obligationId;


describe('Task update tests', async function () {

    before('create obligation', async function () {
        let response = {};
        try {
            response = await axios.post(`${server}/${clientKey}/package/powerhub/compliance/api/create/contract.js`, createObligation.Request.allParameters);
            obligationId = response.data._id;
        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;
        }
        // Verify if the test was successful.
        assert(response.status == 200, `Request to create obligation with all the fields is failing : ${response.status} `);
    });

    beforeEach('create task', async function () {
        let response = {};
        try {
            //Update new ids for links before sending request
            createTask.Request.mandatoryFields.links[0]._id = obligationId;
            response = await axios.post(`${server}/${clientKey}/package/powerhub/compliance/api/create/task.js`, createTask.Request.mandatoryFields);
            taskId = response.data._id;
        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;
        }
        // Verify if the test was successful.
        assert(response.status == 200, `Request to create a task with all the fields is failing : ${response.status} `);
    });

    afterEach('delete tasks, obligations', async function () {
        await deleteOne("tasks", { "_objectType": "task", "_id": ObjectId(taskId) });

    });

    after('delete project, contact', async function () {
        await deleteOne("compliance", { "_objectType": "contract", "_id": ObjectId(obligationId) });
        await deleteOne("contacts", { "_objectType": "contact", "_id": ObjectId(contactId) });
    });



    it('1. update task updateName_Description_NotificationDays', async function () {
        let response = {};
        try {
            //Update new ids for links before sending request
            updateTask.Request.updateName_Description_NotificationDays._id = taskId;
            updateTask.Request.updateName_Description_NotificationDays.links[0]._id = obligationId;
            updateTask.Request.updateName_Description_NotificationDays.links[0]._relatedTo = taskId;
            response = await axios.post(`${server}/${clientKey}/package/powerhub/compliance/api/update/task.js`, updateTask.Request.updateName_Description_NotificationDays);
            id = response.data._id;
            var actual = await removePropFromJSON(response.data, ["_vendorKey", "_clientKey", "_ownerKey", "_dateTimeCreated", "_createdBy", "_dateTimeModified", "_modifiedBy", "_signature"])
        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;

        }
        // update values in expected response to match with actual response
        updateTask.Response.updateName_Description_NotificationDays["_id"] = id;
        updateTask.Response.updateName_Description_NotificationDays.links[0]._id = obligationId;
        updateTask.Response.updateName_Description_NotificationDays.links[0]._relatedTo = id;

        // Verify if the test was successful.
        assert(response.status == 200, `Request to update a task updateName_Description_NotificationDays is failing : ${response.status} `);
        deepEqual(actual, updateTask.Response.updateName_Description_NotificationDays); //Verify the whole object
    });


    //FIXME: AssignedTo value in response is missing
    it('2. Update Category, status, assigned to , Priority', async function () {
        let response = {};
        let ContactRequest = {};
        try {

            //Create a contact
            ContactRequest = await axios.post(`${server}/${clientKey}/package/powerhub/contacts/api/create/contact.js`, createContact.validRequest.allParameters);
            contactId = ContactRequest.data._id;

            //Update new ids for links before sending request
            updateTask.Request.Update_Category_status_assignedTo_Priority._id = taskId;
            updateTask.Request.Update_Category_status_assignedTo_Priority.assignedTo[0]._id = contactId;
            updateTask.Request.Update_Category_status_assignedTo_Priority.links[0]._id = obligationId;
            updateTask.Request.Update_Category_status_assignedTo_Priority.links[0]._relatedTo = id;
            response = await axios.post(`${server}/${clientKey}/package/powerhub/compliance/api/update/task.js`, updateTask.Request.Update_Category_status_assignedTo_Priority);
            id = response.data._id;
            var actual = await removePropFromJSON(response.data, ["_vendorKey", "_clientKey", "_ownerKey", "_dateTimeCreated", "_createdBy", "_dateTimeModified", "_modifiedBy", "_signature"])
        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;

        }
        //update values in expected response to match with actual response
        updateTask.Response.Update_Category_status_assignedTo_Priority["_id"] = id;
        updateTask.Response.Update_Category_status_assignedTo_Priority.links[0]._id = obligationId;
        updateTask.Response.Update_Category_status_assignedTo_Priority.links[0]._relatedTo = id;

        // Verify if the test was successful.
        assert(response.status == 200, `Request to update a task  Category, status, assigned to , Priority is failing : ${response.status} `);
        deepEqual(actual, updateTask.Response.Update_Category_status_assignedTo_Priority); //Verify the whole object
    });


    it('3. update start, end completion date', async function () {
        let response = {};
        try {
            //Update new ids for links in request body before sending request
            updateTask.Request.Update_startEndCompletion_date._id = taskId;
            updateTask.Request.Update_startEndCompletion_date.links[0]._id = obligationId;
            updateTask.Request.Update_startEndCompletion_date.links[0]._relatedTo = taskId;
            response = await axios.post(`${server}/${clientKey}/package/powerhub/compliance/api/update/task.js`, updateTask.Request.Update_startEndCompletion_date);
            id = response.data._id;
            var actual = await removePropFromJSON(response.data, ["_vendorKey", "_clientKey", "_ownerKey", "_dateTimeCreated", "_createdBy", "_dateTimeModified", "_modifiedBy", "_signature"])
        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;
        }

        //  update values in expected response to match with actual response
        updateTask.Response.Update_startEndCompletion_date["_id"] = id;
        updateTask.Response.Update_startEndCompletion_date.links[0]._id = obligationId;
        updateTask.Response.Update_startEndCompletion_date.links[0]._relatedTo = id;

        // Verify if the test was successful.
        assert(response.status == 200, `Request to update a task update start, end completion date is failing : ${response.status} `);
        deepEqual(actual, updateTask.Response.Update_startEndCompletion_date); //Verify the whole object
    });


    it('4. update monthly recurrence end date', async function () {
        let response, monthlyResponse = {};
        try {

            //create a monthly recurring task
            createTask.Request.monthlyRecurrence.links[0]._id = obligationId;
            monthlyResponse = await axios.post(`${server}/${clientKey}/package/powerhub/compliance/api/create/task.js`, createTask.Request.monthlyRecurrence);
            var monthlyTaskId = monthlyResponse.data._id;
            //Update task
            updateTask.Request.update_monthlyRecurrenceEndDate._id = monthlyTaskId;
            updateTask.Request.update_monthlyRecurrenceEndDate.links[0]._id = obligationId;
            updateTask.Request.update_monthlyRecurrenceEndDate.links[0]._relatedTo = monthlyTaskId;
            response = await axios.post(`${server}/${clientKey}/package/powerhub/compliance/api/update/task.js`, updateTask.Request.update_monthlyRecurrenceEndDate);
            id = response.data[0]._id;
            var actual = await removePropFromJSON(response.data[0], ["_vendorKey", "_clientKey", "_ownerKey", "_dateTimeCreated", "_createdBy", "_dateTimeModified", "_modifiedBy", "_signature"])
        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;

        }

        // update values in expected response to match with actual response
        updateTask.Response.update_monthlyRecurrenceEndDate["_id"] = id;
        updateTask.Response.update_monthlyRecurrenceEndDate.links[0]._relatedTo = id;
        updateTask.Response.update_monthlyRecurrenceEndDate.links[0]._id = obligationId;

        // Verify if the test was successful.
        assert(response.status == 200, `Request to update a task monthly recurrence is failing : ${response.status} `);
        deepEqual(actual, updateTask.Response.update_monthlyRecurrenceEndDate); //Verify the whole object
    });

    it('5. update daily recurrence start on', async function () {
        let response, dailyResponse = {};
        try {

            //Create a daily recurrence task
            createTask.Request.dailyRecurrence.links[0]._id = obligationId;
            dailyResponse = await axios.post(`${server}/${clientKey}/package/powerhub/compliance/api/create/task.js`, createTask.Request.dailyRecurrence);
            var dailyTaskId = dailyResponse.data._id;

            //Update new ids for links in request body before sending request
            updateTask.Request.update_dailyRecurrenceStartDate._id = dailyTaskId;
            updateTask.Request.update_dailyRecurrenceStartDate.links[0]._relatedTo = dailyTaskId;
            response = await axios.post(`${server}/${clientKey}/package/powerhub/compliance/api/update/task.js`, updateTask.Request.update_dailyRecurrenceStartDate);
            id = response.data[0]._id;
            var actual = await removePropFromJSON(response.data[0], ["_vendorKey", "_clientKey", "_ownerKey", "_dateTimeCreated", "_createdBy", "_dateTimeModified", "_modifiedBy", "_signature"])
        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;
        }

        //  update values in expected response to match with actual response
        updateTask.Response.update_dailyRecurrenceStartDate["_id"] = id;
        updateTask.Response.update_dailyRecurrenceStartDate.links[0]._relatedTo = id;

        // Verify if the test was successful.
        assert(response.status == 200, `Request to update a task daily recurrence start on is failing : ${response.status} `);
        deepEqual(actual, updateTask.Response.update_dailyRecurrenceStartDate); //Verify the whole object
    });

});