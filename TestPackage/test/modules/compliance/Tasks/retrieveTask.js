process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0;
const { axios, assert, ObjectId } = require('../../../lib/mochalibs').mochalibs;
const { server, clientKey } = require('../../../lib/dbconfig').dbconfig;
const { createObligation, retrieveTask, createTask } = require('../testdata/testdatatasks').testData;
const { removePropFromJSON, deepEqual } = require('../../../utils/mochautils').mochautils;
const { deleteOne } = require('../../../utils/dbwrapper').dbwrapper;
var id, taskId, contactId, obligationId;

describe('Task retrieve tests', async function () {

    before('create obligation', async function () {
        let response = {};
        try {
            response = await axios.post(`${server}/${clientKey}/package/powerhub/compliance/api/create/contract.js`, createObligation.Request.allParameters);
            obligationId = response.data._id;
        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;
        }
        // Verify if the test was successful.
        assert(response.status == 200, `Request to create obligation with all the fields is failing : ${response.status} `);
    });

    beforeEach('create task', async function () {
        let response = {};
        try {

            createTask.Request.mandatoryFields.links[0]._id = obligationId;
            response = await axios.post(`${server}/${clientKey}/package/powerhub/compliance/api/create/task.js`, createTask.Request.mandatoryFields);
            taskId = response.data._id;
        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;

        }
        // Verify if the test was successful.
        assert(response.status == 200, `Request to create a task with all the fields is failing : ${response.status} `);
    });


    afterEach('delete tasks, obligations', async function () {
        await deleteOne("tasks", { "_objectType": "task", "_id": ObjectId(taskId) });

    });

    after('delete project, contact', async function () {
        await deleteOne("compliance", { "_objectType": "contract", "_id": ObjectId(obligationId) });
        await deleteOne("contacts", { "_objectType": "contact", "_id": ObjectId(contactId) });
    });




    it('1.Retrieve a task', async function () {
        let response = {};
        try {
            retrieveTask.Request.retrieveById._id = taskId;
            response = await axios.post(`${server}/${clientKey}/package/powerhub/compliance/api/retrieve/task.js`, retrieveTask.Request.retrieveById);
            id = response.data._id;
            var actual = await removePropFromJSON(response.data, ["_vendorKey", "_clientKey", "_ownerKey", "_dateTimeCreated", "_createdBy", "_dateTimeModified", "_modifiedBy", "_signature"])
        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;

        }
        retrieveTask.Response.retrieveById["_id"] = id;
        retrieveTask.Response.retrieveById.links[0]._id = obligationId;
        retrieveTask.Response.retrieveById.links[0]._relatedTo = id;
        // Verify if the test was successful.
        assert(response.status == 200, `Request to update a task updateName_Description_NotificationDays is failing : ${response.status} `);
        deepEqual(actual, retrieveTask.Response.retrieveById); //Verify the whole object
    });






});