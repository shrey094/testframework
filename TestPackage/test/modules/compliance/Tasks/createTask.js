process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0;
const { axios, assert, ObjectId } = require('../../../lib/mochalibs').mochalibs;
const { server, clientKey } = require('../../../lib/dbconfig').dbconfig;
const { createObligation, createTask, createProject, createProjectGroup } = require('../testdata/testdatatasks').testData;
const { removePropFromJSON, deepEqual } = require('../../../utils/mochautils').mochautils;
const { deleteOne } = require('../../../utils/dbwrapper').dbwrapper;
var id, project, projectId, obligationId;


describe('Task create tests', async function () {

    beforeEach('create obligation', async function () {
        let response = {};
        try {
            response = await axios.post(`${server}/${clientKey}/package/powerhub/compliance/api/create/contract.js`, createObligation.Request.allParameters);
            obligationId = response.data._id;
        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;
        }
        // Verify if the test was successful.
        assert(response.status == 200, `Request to create obligation with all the fields is failing : ${response.status} `);

    });

    before('create project', async function () {
        let response = {};

        try {
            var projectKey = "projectcreate-" + Math.random().toString(36).substring(2, 7);
            createProject.input.projectKey = projectKey;
            response = await axios.post(`${server}/${clientKey}/package/powerhub/projectportfolio/api/create/project.js`, createProject.input);
            projectId = response.data._id
            project = await removePropFromJSON(response.data, ["_vendorKey", "_clientKey", "_ownerKey", "_dateTimeCreated", "_createdBy", "_dateTimeModified", "_modifiedBy", "_signature"])
        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;
        }
        assert(response.status == 200, `Request to create project with all the fields is failing: ${response.status} `);
    });

    afterEach('delete tasks, obligations', async function () {
        await deleteOne("tasks", { "_objectType": "task", "_id": ObjectId(id) });
        await deleteOne("compliance", { "_objectType": "contract", "_id": ObjectId(obligationId) });
    });

    after('delete project, contact', async function () {
        await deleteOne("contacts", { "_objectType": "contact", "_id": ObjectId(contactId) });
        await deleteOne("projects", { "_objectType": "project", "_id": ObjectId(projectId) });
    });



    it('1. create task', async function () {
        let response = {};
        try {
            createTask.Request.allParameters.links[0]._id = obligationId
            response = await axios.post(`${server}/${clientKey}/package/powerhub/compliance/api/create/task.js`, createTask.Request.allParameters);
            id = response.data._id;
            var actual = await removePropFromJSON(response.data, ["_vendorKey", "_clientKey", "_ownerKey", "_dateTimeCreated", "_createdBy", "_dateTimeModified", "_modifiedBy", "_signature"])
        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;
        }
        //update values in expected response to match with actual response
        createTask.Response.allParameters["_id"] = id;
        createTask.Response.allParameters.links[0]._id = obligationId;
        createTask.Response.allParameters.links[0]._relatedTo = id;

        // Verify if the test was successful.
        assert(response.status == 200, `Request to create obligation with all the fields is failing : ${response.status} `);
        deepEqual(actual, createTask.Response.allParameters); //Verify the whole object
    });

    it('2. Create Tasks with mandatory fields', async function () {
        let response = {};
        try {
            createTask.Request.mandatoryFields.links[0]._id = obligationId;
            response = await axios.post(`${server}/${clientKey}/package/powerhub/compliance/api/create/task.js`, createTask.Request.mandatoryFields);

            id = response.data._id;
            var actual = await removePropFromJSON(response.data, ["_vendorKey", "_clientKey", "_ownerKey", "_dateTimeCreated", "_createdBy", "_dateTimeModified", "_modifiedBy", "_signature"]);
        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;
        }
        //update values in expected response to match with actual response
        createTask.Response.mandatoryFields["_id"] = id;
        createTask.Response.mandatoryFields.links[0]._id = obligationId;
        createTask.Response.mandatoryFields.links[0]._relatedTo = id;
        // Verify if the test was successful.
        assert(response.status == 200, `Request to create obligation with all the fields is failing : ${response.status} `);
        deepEqual(actual, createTask.Response.mandatoryFields); //Verify the whole object
    });

    it('3. Create Tasks and assign to Project', async function () {
        let response = {};
        let actual = {};
        try {
            createTask.Request.withProjectLinks.links[0]._id = obligationId;
            createTask.Request.withProjectLinks.links[1]._id = projectId
            response = await axios.post(`${server}/${clientKey}/package/powerhub/compliance/api/create/task.js`, createTask.Request.withProjectLinks);
            id = response.data._id;
            actual = await removePropFromJSON(response.data, ["_vendorKey", "_clientKey", "_ownerKey", "_dateTimeCreated", "_createdBy", "_dateTimeModified", "_modifiedBy", "_signature"]);
        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;
        }
        //update values in expected response to match with actual response
        createTask.Response.withProjectLinks["_id"] = id;
        createTask.Response.withProjectLinks.links[0]._relatedTo = id;
        createTask.Response.withProjectLinks.links[1]._id = obligationId;
        createTask.Response.withProjectLinks.links[0]._id = projectId;
        createTask.Response.withProjectLinks.links[1]._relatedTo = id;
        createTask.Response.withProjectLinks.links[0].projectKey = project.projectKey;

        // Verify if the test was successful.
        assert(response.status == 200, "Create Task and assign to Project is failing : ${response.status} `);
        deepEqual(actual, createTask.Response.withProjectLinks); //Verify the whole object});
    });

    it('4. Create Task and assign to Project group', async function () {
        let response = {};
        let actual = {};
        let projectGroupRequest = {};
        try {

            // Create a project group
            var projectGroupKey = "project_group_create-" + Math.random().toString(36).substring(2, 7);
            createProjectGroup.Request.allParameters.projectGroupKey = projectGroupKey;
            projectGroupRequest = await axios.post(`${server}/${clientKey}/package/powerhub/projectportfolio/api/create/group.js`, createProjectGroup.Request.allParameters);
            var projectGroup = await removePropFromJSON(projectGroupRequest.data, ["_vendorKey", "_clientKey", "_ownerKey", "_dateTimeCreated", "_createdBy", "_dateTimeModified", "_modifiedBy", "_signature"])
            var projectGroupId = projectGroup._id;
            //Create task
            createTask.Request.withProjectGroup.links[0]._id = obligationId;
            createTask.Request.withProjectGroup.links[1]._id = projectGroupId;
            response = await axios.post(`${server}/${clientKey}/package/powerhub/compliance/api/create/task.js`, createTask.Request.withProjectGroup);
            id = response.data._id;
            actual = await removePropFromJSON(response.data, ["_vendorKey", "_clientKey", "_ownerKey", "_dateTimeCreated", "_createdBy", "_dateTimeModified", "_modifiedBy", "_signature"])
        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;

        }
        //update values in expected response to match with actual response
        createTask.Response.withProjectGroup["_id"] = id;
        createTask.Response.withProjectGroup.links[0]._id = projectGroupId;
        createTask.Response.withProjectGroup.links[0]._relatedTo = id;
        createTask.Response.withProjectGroup.links[0].projectGroupKey = projectGroup.projectGroupKey;
        createTask.Response.withProjectGroup.links[1]._id = obligationId;
        createTask.Response.withProjectGroup.links[1]._relatedTo = id;

        // Verify if the test was successful.
        assert(response.status == 200, `Request to create obligation with all the fields is failing : ${response.status} `);
        deepEqual(actual, createTask.Response.withProjectGroup); //Verify the whole object

    });


    it('5. create Task and assign to none', async function () {
        let response = {};
        try {
            createTask.Request.assignToNone.links[0]._id = obligationId;
            response = await axios.post(`${server}/${clientKey}/package/powerhub/compliance/api/create/task.js`, createTask.Request.assignToNone);
            id = response.data._id;
            var actual = await removePropFromJSON(response.data, ["_vendorKey", "_clientKey", "_ownerKey", "_dateTimeCreated", "_createdBy", "_dateTimeModified", "_modifiedBy", "_signature"])
        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;

        }
        // update values in expected response to match with actual response
        createTask.Response.assignToNone["_id"] = id;
        createTask.Response.assignToNone.links[0]._id = obligationId;
        createTask.Response.assignToNone.links[0]._relatedTo = id;
        // Verify if the test was successful.
        assert(response.status == 200, `Request to  create Task and assign to none failed: ${response.status}`);
        deepEqual(actual, createTask.Response.assignToNone); //Verify the whole object
    });

    it('6. Create daily recurrence task', async function () {
        let response = {};
        try {

            createTask.Request.dailyRecurrence.links[0]._id = obligationId;
            response = await axios.post(`${server}/${clientKey}/package/powerhub/compliance/api/create/task.js`, createTask.Request.dailyRecurrence);
            id = response.data._id;
            var actual = await removePropFromJSON(response.data, ["_vendorKey", "_clientKey", "_ownerKey", "_dateTimeCreated", "_createdBy", "_dateTimeModified", "_modifiedBy", "_signature"])
        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;

        }
        //update values in expected response to match with actual response
        createTask.Response.dailyRecurrence["_id"] = id;
        createTask.Response.dailyRecurrence.links[0]._relatedTo = id;
        createTask.Response.dailyRecurrence.links[0]._id = obligationId;
        createTask.Response.dailyRecurrence.recurrence.recurrenceId = actual.recurrence.recurrenceId
        // Verify if the test was successful.
        assert(response.status == 200, `Request to create obligation with all the fields is failing: ${response.status} `);
        deepEqual(actual, createTask.Response.dailyRecurrence); //Verify the whole object
    });

    it('7. Create weekly recurrence task', async function () {
        let response = {};
        try {

            createTask.Request.weeklyRecurrence.links[0]._id = obligationId;
            response = await axios.post(`${server} /${clientKey}/package/powerhub/compliance/api/create/task.js`, createTask.Request.weeklyRecurrence);
            id = response.data._id;
            var actual = await removePropFromJSON(response.data, ["_vendorKey", "_clientKey", "_ownerKey", "_dateTimeCreated", "_createdBy", "_dateTimeModified", "_modifiedBy", "_signature"])
        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;

        }
        //update values in expected response to match with actual response
        createTask.Response.weeklyRecurrence["_id"] = id;
        createTask.Response.weeklyRecurrence.links[0]._relatedTo = id;
        createTask.Response.weeklyRecurrence.links[0]._id = obligationId;
        createTask.Response.weeklyRecurrence.recurrence.recurrenceId = actual.recurrence.recurrenceId
        // Verify if the test was successful.
        assert(response.status == 200, `Request to create obligation with all the fields is failing: ${response.status} `);
        deepEqual(actual, createTask.Response.weeklyRecurrence); //Verify the whole object
    });


    it('8. Create monthly recurrence task', async function () {
        let response = {};
        try {

            createTask.Request.monthlyRecurrence.links[0]._id = obligationId;
            response = await axios.post(`${server} /${clientKey}/package/powerhub/compliance/api/create/task.js`, createTask.Request.monthlyRecurrence);
            id = response.data._id;
            var actual = await removePropFromJSON(response.data, ["_vendorKey", "_clientKey", "_ownerKey", "_dateTimeCreated", "_createdBy", "_dateTimeModified", "_modifiedBy", "_signature"])
        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;

        }
        //update values in expected response to match with actual response
        createTask.Response.monthlyRecurrence["_id"] = id;
        createTask.Response.monthlyRecurrence.links[0]._relatedTo = id;
        createTask.Response.monthlyRecurrence.links[0]._id = obligationId;
        createTask.Response.monthlyRecurrence.recurrence.recurrenceId = actual.recurrence.recurrenceId
        // Verify if the test was successful.
        assert(response.status == 200, `Request to create obligation with all the fields is failing: ${response.status} `);
        deepEqual(actual, createTask.Response.monthlyRecurrence); //Verify the whole object
    });


    it('9. Create yearly recurrence task', async function () {
        let response = {};
        try {

            createTask.Request.yearlyRecurrence.links[0]._id = obligationId;
            response = await axios.post(`${server} /${clientKey}/package/powerhub/compliance/api/create/task.js`, createTask.Request.yearlyRecurrence);
            id = response.data._id;
            var actual = await removePropFromJSON(response.data, ["_vendorKey", "_clientKey", "_ownerKey", "_dateTimeCreated", "_createdBy", "_dateTimeModified", "_modifiedBy", "_signature"])
        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;

        }
        //update values in expected response to match with actual response
        createTask.Response.yearlyRecurrence["_id"] = id;
        createTask.Response.yearlyRecurrence.links[0]._relatedTo = id;
        createTask.Response.yearlyRecurrence.links[0]._id = obligationId;
        createTask.Response.yearlyRecurrence.recurrence.recurrenceId = actual.recurrence.recurrenceId
        // Verify if the test was successful.
        assert(response.status == 200, `Request to create obligation with all the fields is failing: ${response.status} `);
        deepEqual(actual, createTask.Response.yearlyRecurrence); //Verify the whole object
    });

});