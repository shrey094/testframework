process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0;
const { axios, assert } = require('../../../lib/mochalibs').mochalibs;
const { server, clientKey } = require('../../../lib/dbconfig').dbconfig;
const { createObligation, createTask } = require('../testdata/testdatatasks').testData;
const { deepEqual } = require('../../../utils/mochautils').mochautils;
var taskId, obligationId;

describe('Task delete tests', async function () {

    before('create obligation', async function () {
        let response = {};
        try {
            response = await axios.post(`${server}/${clientKey}/package/powerhub/compliance/api/create/contract.js`, createObligation.Request.allParameters);
            obligationId = response.data._id;
        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;
        }
        // Verify if the test was successful.
        assert(response.status == 200, `Request to create obligation with all the fields is failing : ${response.status} `);
    });

    beforeEach('create task', async function () {
        let response = {};
        try {
            debugger;
            createTask.Request.mandatoryFields.links[0]._id = obligationId;
            response = await axios.post(`${server}/${clientKey}/package/powerhub/compliance/api/create/task.js`, createTask.Request.mandatoryFields);
            taskId = response.data._id;
        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;

        }
        // Verify if the test was successful.
        assert(response.status == 200, `Request to create a task with all the fields is failing : ${response.status} `);
    });

    afterEach('delete tasks', async function () {
        await deleteOne("tasks", { "_objectType": "task", "_id": ObjectId(id) });

    });

    after('delete obligation', async function () {
        await deleteOne("compliance", { "_objectType": "contract", "_id": ObjectId(obligationId) });
    });




    it('1.delete a task', async function () {
        let response = {};
        let request = { "_id": "", "type": "task" }
        try {
            request._id = taskId;
            response = await axios.post(`${server}/${clientKey}/package/powerhub/compliance/api/delete/task.js`, request);
        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;
        }
        // Verify if the test was successful.
        assert(response.status == 200, `Request to update a task updateName_Description_NotificationDays is failing : ${response.status} `);
        deepEqual(response.data, { "deleted": 1 });
    });






});