process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0;
const { axios, assert, ObjectId } = require('../../lib/mochalibs').mochalibs;
const { server, clientKey } = require('../../lib/dbconfig').dbconfig;
const { createProject, createSolarProjectGroup, createWindProjectGroup, createProjectWithMandatoryFields, createProjectWithDuplicatekey,
    createProjectwithInvalidObjectType, createProjectwithInvalidId, createProjectWithInvalidValues, createProjectToCheckDefaultVal,
    createProjectWithOutMandatoryFields, createProjectWithOutLinks, createProjectWithtimeZone, createProjectWithProjectGroupAssigned, createProjectWithMultiProjectGroupAssigned } = require('./testdata/testdataprojects').testData;
const { removePropFromJSON, deepEqual } = require('../../utils/mochautils').mochautils;
const { deleteOne } = require('../../utils/dbwrapper').dbwrapper;
const { assignProjectKey, generateProjectKey, getTodayDateFormatted } = require('../../utils/conversionutils').dateutils;
var id;
var projectGroupId_Solar, projectGroupId_Wind;
var projectGroupKey_Solar, projectGroupKey_Wind;


describe.only('Projects create tests', async function () {

    beforeEach('create projects', async function () {
    });

    before('create projects', async function () {
        let response = {};
        try {
            projectGroupKey_Solar = generateProjectKey("ProjectGroupSolar")
            createSolarProjectGroup.input.projectGroupKey = projectGroupKey_Solar
            response = await axios.post(`https://z0.powerhub.com/${clientKey}/package/powerhub/projectportfolio/api/create/group.js`, createSolarProjectGroup.input);
            projectGroupId_Solar = response.data._id
            projectGroupKey_Wind = generateProjectKey("ProjectGroupWind")
            createWindProjectGroup.input.projectGroupKey = projectGroupKey_Wind
            response = await axios.post(`https://z0.powerhub.com/${clientKey}/package/powerhub/projectportfolio/api/create/group.js`, createWindProjectGroup.input);
            projectGroupId_Wind = response.data._id
        }
        catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;
        }
    });


    it('1. Verify create project with valid values in all the fields', async function () {
        let response;
        let actual;
        var changeName = generateProjectKey("ProjectCreate1")
        createProject.input.projectKey = changeName;
        createProject.expected.projectKey = createProject.input.projectKey
        try {
            response = await axios.post(`https://z0.powerhub.com/${clientKey}/package/powerhub/projectportfolio/api/create/project.js`, createProject.input);
            id = response.data._id
            actual = await removePropFromJSON(response.data, ["_vendorKey", "_clientKey", "_ownerKey", "_dateTimeCreated", "_createdBy", "_dateTimeModified", "_modifiedBy", "_signature"])
        }
        catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;
        }
        createProject.expected["_id"] = id;
        assert(response.status == 200, "Status code is not 200, unable to retrieve the project by id");
        deepEqual(actual, createProject.expected, "Response object is not same as expected")

        await deleteOne("projects", { "_objectType": "project", "_id": ObjectId(id) });
        //console.log(x)
    });

    it('2. Verify create project with ONLY MANDATORY FIELDS', async function () {
        let response = {};
        let actual;
        changeName = generateProjectKey("ProjectCreate2")
        createProjectWithMandatoryFields.input.projectKey = changeName;
        createProjectWithMandatoryFields.expected.projectKey = createProjectWithMandatoryFields.input.projectKey
        try {
            response = await axios.post(`https://z0.powerhub.com/${clientKey}/package/powerhub/projectportfolio/api/create/project.js`, createProjectWithMandatoryFields.input);
            id = response.data._id
            actual = await removePropFromJSON(response.data, ["_vendorKey", "startDate", "_clientKey", "_ownerKey", "_dateTimeCreated", "_createdBy", "_dateTimeModified", "_modifiedBy", "_signature"])
        }
        catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;
        }
        createProjectWithMandatoryFields.expected["_id"] = id;
        assert(response.status == 200, `Request to create project with ONLY Mandatory fields is failing: ${response.status} `);
        deepEqual(actual, createProjectWithMandatoryFields.expected, "Response object is not same as expected")
    })

    it('3. Verify UNIQUE Project Key validation', async function () {

        let response = {};
        try {
            response = await axios.post(`https://z0.powerhub.com/${clientKey}/package/powerhub/projectportfolio/api/create/project.js`, createProjectWithDuplicatekey.createProject);
            response = await axios.post(`https://z0.powerhub.com/${clientKey}/package/powerhub/projectportfolio/api/create/project.js`, createProjectWithDuplicatekey.input);
        }
        catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;
        }
        assert(response.status == 500, `Request to create project is not failing - ${response.status}`);
        //test.assert(response.data == "Duplicate projectKey projecttestwithduplicatekey", `Data: ${response.data}`)   //Verify the whole object
    })

    it('4. Verify create project with invalid id in reference nodes for contacts', async function () {
        let response = {};
        let actual;
        changeName = generateProjectKey("ProjectCreate4")
        createProjectwithInvalidId.input.projectKey = changeName;
        createProjectwithInvalidId.expected.projectKey = createProjectwithInvalidId.input.projectKey
        try {
            response = await axios.post(`https://z0.powerhub.com/${clientKey}/package/powerhub/projectportfolio/api/create/project.js`, createProjectwithInvalidId.input);
            id = response.data._id
            actual = await removePropFromJSON(response.data, ["_vendorKey", "_clientKey", "_ownerKey", "_dateTimeCreated", "_createdBy", "_dateTimeModified", "_modifiedBy", "_signature"])
        }
        catch (e) {
            response.status = e.response.status;
        }
        createProjectwithInvalidId.expected["_id"] = id;
        assert(response.status == 200, "Request to create project with invalid contact ids is failing");
        deepEqual(actual, createProjectwithInvalidId.expected)   //Verify the whole object
    })

    it('5. Verify create project with invalid object type in reference nodes for contacts', async function () {
        let response = {};
        let actual;
        changeName = generateProjectKey("ProjectCreate4")
        createProjectwithInvalidObjectType.input.projectKey = changeName;
        createProjectwithInvalidObjectType.expected.projectKey = createProjectwithInvalidObjectType.input.projectKey
        try {
            response = await axios.post(`https://z0.powerhub.com/${clientKey}/package/powerhub/projectportfolio/api/create/project.js`, createProjectwithInvalidObjectType.input);
            id = response.data._id
            actual = await removePropFromJSON(response.data, ["_vendorKey", "_clientKey", "_ownerKey", "_dateTimeCreated", "_createdBy", "_dateTimeModified", "_modifiedBy", "_signature"])
        }
        catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;
        }
        createProjectwithInvalidObjectType.expected["_id"] = id;
        assert(response.status == 200, `Request to create project with invalid objectType is failing - ${response.status}`);
        deepEqual(actual, createProjectwithInvalidObjectType.expected, `data - ${response.data}`)   //Verify the whole object
    })

    it('6. Verify validations for startDate, coordinates, size, status,financialYearEnd, business days and country', async function () {
        let response = {};
        try {
            response = await axios.post(`https://z0.powerhub.com/${clientKey}/package/powerhub/projectportfolio/api/create/project.js`, createProjectWithInvalidValues.input);
        }
        catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;
        }
        assert(response.status == 400, "Request to create project is not failing");
        deepEqual(response.data, createProjectWithInvalidValues.expected, `data: ${response.data}`)   //Verify the whole object

    })

    it('7. Verify default values', async function () {
        let response = {};
        changeName = generateProjectKey("ProjectCreate7")
        createProjectToCheckDefaultVal.input.projectKey = changeName;
        createProjectToCheckDefaultVal.expected.projectKey = createProjectToCheckDefaultVal.input.projectKey

        try {
            response = response = await axios.post(`https://z0.powerhub.com/${clientKey}/package/powerhub/projectportfolio/api/create/project.js`, createProjectToCheckDefaultVal.input);
            id = response.data._id
        }
        catch (e) {
            response.status = e.response.status;
        }
        let todaysDate = await getTodayDateFormatted();

        assert(response.status == 200, `Request to create project with ONLY Mandatory fields is failing ${response.status}`);
        assert.equal(response.data.financialYearEnd, 12, "FinancialYearEnd is not same")   //verifying the financial year end
        assert.deepEqual(response.data.businessDays, ["monday", "tuesday", "wednesday", "thursday", "friday"], "BusinessDays are not same")   //verifying the links
        assert.equal(response.data.size.unit, "kW", "Size.Unit is not same")   //verifying the size unit
        assert.equal(response.data.geoLocation.type, "Point", "GeoLocation.Type is not same")   //verifying the geolocation coordinates
        //assert.equal(response.data.startDate.substring(0, 10), todaysDate, "StartDate is not same")   //verifying the start date
    })

    it('8. Verify create project with links and WITHOUT MANDATORY FIELDS', async function () {
        let response = {};
        try {
            response = response = await axios.post(`https://z0.powerhub.com/${clientKey}/package/powerhub/projectportfolio/api/create/project.js`, createProjectWithOutMandatoryFields.input);
        }
        catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;
        }
        assert(response.status == 400, `Request to create project is not failing- ${response.status}`);
        deepEqual(response.data, createProjectWithOutMandatoryFields.expected, `data: ${response.data}`)   //Verify the whole object
    })

    it('9. Verify create project WITHOUT MANDATORY FIELDS and links', async function () {
        let response = {};
        try {
            response = response = await axios.post(`https://z0.powerhub.com/${clientKey}/package/powerhub/projectportfolio/api/create/project.js`, createProjectWithOutLinks.input);
        }
        catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;
        }
        assert(response.status == 500, `Request to create project is not failing- ${response.status}`);
        assert(response.data == "Cannot read property 'filter' of undefined", `Data- ${response.data}`)   //Verify the whole object
    })

    it('10. Verify create project with timezone', async function () {
        let response = {};
        let actual;
        changeName = generateProjectKey("ProjectCreate10")
        createProjectWithtimeZone.input.projectKey = changeName;
        createProjectWithtimeZone.expected.projectKey = createProjectWithtimeZone.input.projectKey
        try {
            response = response = await axios.post(`https://z0.powerhub.com/${clientKey}/package/powerhub/projectportfolio/api/create/project.js`, createProjectWithtimeZone.input);
            id = response.data._id
            actual = await removePropFromJSON(response.data, ["_vendorKey", "_clientKey", "_ownerKey", "_dateTimeCreated", "_createdBy", "_dateTimeModified", "_modifiedBy", "_signature"])
        }
        catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;
        }
        createProjectWithtimeZone.expected["_id"] = id;
        assert(response.status == 200, `Request to create project with time zone is failing - ${response.status}`);
        deepEqual(actual, createProjectWithtimeZone.expected, `data - ${response.data}`)   //Verify the whole object
    })

    it('11. Verify create project and assign it to single project group', async function () {
        let response = {};
        let actual;
        var changeName = generateProjectKey("ProjectCreate11")
        createProjectWithProjectGroupAssigned.input.projectKey = changeName;
        createProjectWithProjectGroupAssigned.expected.projectKey = createProjectWithProjectGroupAssigned.input.projectKey
        //Adding Project Group ids to the links project input and expected
        createProjectWithProjectGroupAssigned.input.links[0]["_id"] = projectGroupId_Solar
        createProjectWithProjectGroupAssigned.expected.links[0]["_id"] = projectGroupId_Solar

        createProjectWithProjectGroupAssigned.expected.links[0]["projectGroupKey"] = projectGroupKey_Solar

        try {
            response = response = await axios.post(`https://z0.powerhub.com/${clientKey}/package/powerhub/projectportfolio/api/create/project.js`, createProjectWithProjectGroupAssigned.input);
            id = response.data._id
            actual = await removePropFromJSON(response.data, ["_vendorKey", "_clientKey", "_ownerKey", "_dateTimeCreated", "_createdBy", "_dateTimeModified", "_modifiedBy", "_signature"])
        }
        catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;
        }
        createProjectWithProjectGroupAssigned.expected["_id"] = id;
        createProjectWithProjectGroupAssigned.expected.links[0]["_relatedTo"] = id;

        assert(response.status == 200, "Request to create project with all the fields is failing");
        deepEqual(actual, createProjectWithProjectGroupAssigned.expected)   //Verify the whole object

    })

    it('Verify create project and assign it to multiple project groups', async function () {
        let response = {};
        let actual;
        var changeName = generateProjectKey("ProjectCreate12")
        createProjectWithMultiProjectGroupAssigned.input.projectKey = changeName;
        createProjectWithMultiProjectGroupAssigned.expected.projectKey = createProjectWithMultiProjectGroupAssigned.input.projectKey

        createProjectWithMultiProjectGroupAssigned.input.links[0]["_id"] = projectGroupId_Solar
        createProjectWithMultiProjectGroupAssigned.input.links[1]["_id"] = projectGroupId_Wind

        createProjectWithMultiProjectGroupAssigned.expected.links[0]["_id"] = projectGroupId_Solar
        createProjectWithMultiProjectGroupAssigned.expected.links[1]["_id"] = projectGroupId_Wind

        createProjectWithMultiProjectGroupAssigned.expected.links[0]["projectGroupKey"] = projectGroupKey_Solar
        createProjectWithMultiProjectGroupAssigned.expected.links[1]["projectGroupKey"] = projectGroupKey_Wind

        try {
            response = response = await axios.post(`https://z0.powerhub.com/${clientKey}/package/powerhub/projectportfolio/api/create/project.js`, createProjectWithMultiProjectGroupAssigned.input);
            id = response.data._id
            actual = await removePropFromJSON(response.data, ["_vendorKey", "_clientKey", "_ownerKey", "_dateTimeCreated", "_createdBy", "_dateTimeModified", "_modifiedBy", "_signature"])
        }
        catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;
        }
        createProjectWithMultiProjectGroupAssigned.expected["_id"] = id;
        createProjectWithMultiProjectGroupAssigned.expected.links[0]["_relatedTo"] = id;
        createProjectWithMultiProjectGroupAssigned.expected.links[1]["_relatedTo"] = id;

        assert(response.status == 200, "Request to create project with all the fields is failing");
        deepEqual(actual, createProjectWithMultiProjectGroupAssigned.expected)   //Verify the whole object
    })


    afterEach('delete projects', async function () {
        await deleteOne("projects", { "_objectType": "project", "_id": ObjectId(id) });
    });
});