process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0;
const { axios, assert } = require('../../../lib/mochalibs').mochalibs;
const { server, clientKey } = require('../../../lib/dbconfig').dbconfig;
const { createContact, createProject } = require('../testdata/testDataContacts').testData;
const { removePropFromJSON, deepEqual } = require('../../../utils/mochautils').mochautils;
var id;
var project;
var projectId;

describe('Contact Create tests', async function () {

    before('Create project', async function () {
        let response = {};
        var projectKey = "projectcreate-" + Math.random().toString(36).substring(2, 7);
        createProject.input.projectKey = projectKey;
        try {
            response = await axios.post(`${server}/${clientKey}/package/powerhub/projectportfolio/api/create/project.js`, createProject.input);
            projectId = response.data._id
            project = await removePropFromJSON(response.data, ["_vendorKey", "_clientKey", "_ownerKey", "_dateTimeCreated", "_createdBy", "_dateTimeModified", "_modifiedBy", "_signature"])
        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;
        }
        assert(response.status == 200, `Request to create project with all the fields is failing: ${response.status} `);
    })

    it('1:Create a valid contact', async function () {
        let response = {};
        try {
            response = await axios.post(`${server}/${clientKey}/package/powerhub/contacts/api/create/contact.js`, createContact.validRequest.allParameters);
            id = response.data._id;
            var actual = await removePropFromJSON(response.data, ["_vendorKey", "_clientKey", "_ownerKey", "_dateTimeCreated", "_createdBy", "_dateTimeModified", "_modifiedBy", "_signature"])
        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;
        }
        //update values in expected response to match with actual response
        createContact.expected.response["_id"] = id;
        // Verify if the test was successful.
        assert(response.status == 200, `Request to create contact with all the fields is failing ${response.status}`);
        deepEqual(actual, createContact.expected.response); //Verify the whole object

    })

    it('Verify create contact With only mandatory first name in body', async function () {

        let response = {};
        let request = {};
        try {
            request = await removePropFromJSON(createContact.validRequest.allParameters, ["isGlobal", "contactMethod", "preferredContactMethod", "position", "company", "groups", "links"]);
            //Change the last name to null and make first and full name equal 
            request.name["lastName"] = null;
            request.name["fullName"] = request.name["firstName"];
            response = await axios.post(`${server}/${clientKey}/package/powerhub/contacts/api/create/contact.js`, request);
            id = response.data._id;
            var actual = await removePropFromJSON(response.data, ["_vendorKey", "_clientKey", "_ownerKey", "_dateTimeCreated", "_createdBy", "_dateTimeModified", "_modifiedBy", "_signature"])
        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;
        }

        createContact.expected.response["_id"] = id;
        createContact.expected.response.name.lastName = null;
        createContact.expected.response.name["firstName"] = request.name["firstName"];
        createContact.expected.response.name["fullName"] = request.name["fullName"];
        createContact.expected.response.links = null;
        // Verify if the test was successful.
        assert(response.status == 200, ` Request to create contact with only first name mandatory field is failing: ${response.status} `);
        deepEqual(actual, createContact.expected.response, `data: ${response.data}`);
    })

    it('Verify create contact without mandatory first name in body', async function () {
        let response = {};
        let body = {
            "_objectType": "contact",
            "type": "individual",
        };
        let expected = {
            "name": "must be present"
        };
        try {
            response = await axios.post(`${server}/${clientKey}/package/powerhub/contacts/api/create/contact.js`, body);
        } catch (e) {

            response.status = e.response.status;
            response.data = e.response.data;
        }

        //Verify the response
        assert(response.status == 400, `Request to create contact without first name is not failing- ${response.status}`);
        deepEqual(response.data, expected, `data: ${response.data}`);
    })

    it('Verify create contact with multiple projects assigned to a contact', async function () {
        let response = {};
        try {
            createContact.validRequest.withProjectLinks.links[0]._id = projectId;
            response = await axios.post(`${server}/${clientKey}/package/powerhub/contacts/api/create/contact.js`, createContact.validRequest.withProjectLinks);
            id = response.data._id;
            var actual = await removePropFromJSON(response.data, ["_vendorKey", "_clientKey", "_ownerKey", "_dateTimeCreated", "_createdBy", "_dateTimeModified", "_modifiedBy", "_signature"])
        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;
        }
        //update values in expected response to match with actual response
        createContact.expected.bodyWithProjectLinks["_id"] = id;
        createContact.expected.bodyWithProjectLinks.links[0]._relatedTo = id;
        createContact.expected.bodyWithProjectLinks.links[0]._id = projectId;
        createContact.expected.bodyWithProjectLinks.links[0].projectKey = project.projectKey;

        //Verify the response
        assert(response.status == 200, `Request to create contact with project links is failing ${response.status}`);
        deepEqual(actual, createContact.expected.bodyWithProjectLinks); //Verify the whole object
    })


    it('Verify create contact with multiple email, more than 2 marked as primary', async function () {
        let response = {};
        let expected = {
            "contactMethod": "there must be only one primary email"
        }
        let contactMethod = {
            "email": [{
                "value": "asd@dd.cd",
                "isPrimary": true
            }, {
                "value": "asd@dd.cd",
                "isPrimary": true
            }],
            "phone": [],
            "fax": []
        };
        try {
            createContact.validRequest.allParameters.contactMethod = contactMethod;
            response = await axios.post(`${server}/${clientKey}/package/powerhub/contacts/api/create/contact.js`, createContact.validRequest.allParameters);
        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;
        }
        //Verify the response
        assert(response.status == 400, `Request to create contact with multiple primary email is not failing ${response.status}`);
        deepEqual(response.data, expected); //Verify the whole object
    })


    it('Verify create a global contact ', async function () {
        let response = {};
        let actual;
        try {
            response = await axios.post(`${server}/${clientKey}/package/powerhub/contacts/api/create/contact.js`, createContact.validRequest.globalContact);
            id = response.data._id
            //  Removing the property to make the response object same as input object for comparison.
            actual = await removePropFromJSON(response.data, ["_vendorKey", "_clientKey", "_ownerKey", "_dateTimeCreated", "_createdBy", "_dateTimeModified", "_modifiedBy", "_signature"])
        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;
        }
        //update values in expected response to match with actual response
        createContact.expected.withResponseGlobalContact["_id"] = id;
        //Verify the response
        assert(response.status == 200, `Request to create a global contact  is failing: ${response.status} `);
        deepEqual(actual, createContact.expected.withResponseGlobalContact); //Verify the whole object
    })

});


