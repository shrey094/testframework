process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0;
const { axios, assert } = require('../../../lib/mochalibs').mochalibs;
const { server, clientKey } = require('../../../lib/dbconfig').dbconfig;
const { retrieveContact, createContact } = require('../testdata/testDataContacts').testData;
const { removePropFromJSON, deepEqual } = require('../../../utils/mochautils').mochautils;
var id;

describe('Retrieve Contacts tests', async function () {
    beforeEach(async function () {
    })


    before('create contact', async function () {

        let response = {};
        try {

            createContact.validRequest.allParameters.contactMethod = null;
            response = await axios.post(`${server}/${clientKey}/package/powerhub/contacts/api/create/contact.js`, createContact.validRequest.allParameters);
            //  Removing the property to make the response object same as input object for comparison.
            id = response.data._id
        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;

        }
        assert(response.status == 200, "Request to create contact with all the fields is failing");
    })

    it('Verify retrieve contact with valid request body (Rule: retrievecontact.js', async function () {

        let response = {};
        let actual;
        try {
            response = await axios.post(`${server}/${clientKey}/package/powerhub/contacts/api/retrieve/contact.js`, { "_id": id });
            //Removing the property to make the response object same as input object for comparison.
            actual = await removePropFromJSON(response.data, ["_vendorKey", "_clientKey", "_ownerKey", "_dateTimeCreated", "_createdBy", "_dateTimeModified", "_modifiedBy", "_signature"])
            id = response.data._id
        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;

        }
        retrieveContact.expectedResponse._id = id;
        console.log(actual)
        console.log(retrieveContact.expectedResponse)
        assert(response.status == 200, "Request to retrieve contact with all the fields is failing");
        deepEqual(actual, retrieveContact.expectedResponse); //Verify the whole object

    });
})