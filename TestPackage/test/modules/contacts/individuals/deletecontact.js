
process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0;
const { axios, assert } = require('../../../lib/mochalibs').mochalibs;
const { server, clientKey } = require('../../../lib/dbconfig').dbconfig;
const { deleteContact, createContact } = require('../testdata/testDataContacts').testData;
const {  deepEqual } = require('../../../utils/mochautils').mochautils;
var id;

describe('Contact Delete tests', async function () {

    beforeEach(async function () { })

    before('create contact', async function () {
        this.enableTimeouts(false)
        let response = {};
        try {
            createContact.validRequest.allParameters.contactMethod = null;
            response = await axios.post(`${server}/${clientKey}/package/powerhub/contacts/api/create/contact.js`, createContact.validRequest.allParameters);
            id = response.data._id
        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;


        }

        createContact.expected.response["_id"] = id;

        assert(response.status == 200, `Request to create contact with all the fields is failing`);
    })

    it('Verify delete contact with valid values in all the fields (Rule: deletecontact.js)', async function () {

        let response = {};
        try {

            response = await axios.post(`${server}/${clientKey}/package/powerhub/contacts/api/delete/contact.js`, {
                "_id": id
            });
        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;

        }

        assert(response.status == 200, `Status code assertion failed: ${response.status}`);
        deepEqual(response.data, deleteContact.expectedResponse); //Verify the whole object

    });
});
