process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0;
const { axios, assert } = require('../../../lib/mochalibs').mochalibs;
const { server, clientKey } = require('../../../lib/dbconfig').dbconfig;
const { createContact, updateContact, createCompany } = require('../testdata/testDataContacts').testData;
const { removePropFromJSON, deepEqual } = require('../../../utils/mochautils').mochautils;
let id;
let contact;
let company;
let companyId;

describe('Contact update tests', async function () {
    beforeEach('Create company', async function () {

        let response = {};
        try {
            createContact.validRequest.allParameters.contactMethod = null;
            response = await axios.post(`${server}/${clientKey}/package/powerhub/contacts/api/create/contact.js`, createContact.validRequest.allParameters);
            id = response.data._id
            //  Removing the property to make the response object same as input object for comparison.
            contact = await removePropFromJSON(response.data, ["_vendorKey", "_clientKey", "_ownerKey", "_dateTimeCreated", "_createdBy", "_dateTimeModified", "_modifiedBy", "_signature"])

        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;

        }

        //update values in expected response to match with actual response
        createContact.expected.response["_id"] = id;
        // Verify if the test was successful.
        assert(response.status == 200, "Request to create contact with all the fields is failing");
        deepEqual(contact, createContact.expected.response); //Verify the whole object


    })

    before('Create project', async function () {
        this.enableTimeouts(false)
        let response = {};
        try {
            createContact.validRequest.allParameters.contactMethod = null;
            response = await axios.post(`${server}/${clientKey}/package/powerhub/contacts/api/create/contact.js`, createCompany.validRequest.allParameters);
            companyId = response.data._id
            //  Removing the property to make the response object same as input object for comparison.
            company = await removePropFromJSON(response.data, ["_vendorKey", "_clientKey", "_ownerKey", "_dateTimeCreated", "_createdBy", "_dateTimeModified", "_modifiedBy", "_signature"])

        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;

        }

        //update values in expected response to match with actual response
        createCompany.response.expected["_id"] = companyId;

        // Verify if the test was successful.
        assert(response.status == 200, "Request to create company contact with all the fields is failing");
        deepEqual(company, createCompany.response.expected); //Verify the whole object

    })

    // Test 1 
    it('update name and position of a contact', async function () {
        let response = {};
        let actual;
        try {
            //Update first name , last name adn position
            contact.name["firstName"] = "Updated";
            contact.name["lastName"] = "name";
            contact.name["fullName"] = "Updated name";
            contact.position = "Tester";
            //send post request to update
            response = await axios.post(`${server}/${clientKey}/package/powerhub/contacts/api/update/contact.js`, contact);
            //remove unnecessary values from response
            actual = await removePropFromJSON(response.data, ["_vendorKey", "_clientKey", "_ownerKey", "_dateTimeCreated", "_createdBy", "_dateTimeModified", "_modifiedBy", "_signature"]);
            id = response.data._id
        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;
        }
        //update the id in expected response
        updateContact.expected.withUpdatedName["_id"] = id;

        //Verify the response
        assert(response.status == 200, "Request to update name and position of a Contact with all the fields is failing");
        deepEqual(actual, updateContact.expected.withUpdatedName);
    });


    // Test 2 
    it('update company and group', async function () {
        let response = {};
        let actual;
        var companylink = {
            "_id": companyId,
            "_objectType": "contact",
            "name": company.name
        };
        try {
            // update company, position and name 
            contact.company[0] = companylink;
            response = await axios.post(`${server}/${clientKey}/package/powerhub/contacts/api/update/contact.js`, contact);
            actual = await removePropFromJSON(response.data, ["_vendorKey", "_clientKey", "_ownerKey", "_dateTimeCreated", "_createdBy", "_dateTimeModified", "_modifiedBy", "_signature"]);
            id = response.data._id
        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;
        }
        //update values in expected response to match with actual response
        updateContact.expected.withUpdatedCompany["_id"] = id;
        updateContact.expected.withUpdatedCompany.company[0]._id = companyId;
        updateContact.expected.withUpdatedCompany.company[0].name = company.name.fullName;
        //Verify the response
        assert(response.status == 200, "Request to update contact company and group with all the fields is failing");
        deepEqual(actual, updateContact.expected.withUpdatedCompany);
    });
    // Test 3
    it('update address', async function () {
        let response = {};
        let actual;
        var address = {
            "addressLine": "111 fake st",
            "city": "Mississauga",
            "postalCode": " L5W 1X9",
            "state": null,
            "country": null
        };
        try {
            // update company, position and name 
            contact.address = address;
            response = await axios.post(`${server}/${clientKey}/package/powerhub/contacts/api/update/contact.js`, contact);
            actual = await removePropFromJSON(response.data, ["_vendorKey", "_clientKey", "_ownerKey", "_dateTimeCreated", "_createdBy", "_dateTimeModified", "_modifiedBy", "_signature"]);
            id = response.data._id

        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;
        }

        //update values in expected response to match with actual response
        updateContact.expected.response["_id"] = id;
        updateContact.expected.response.address = address;

        //Verify the response
        assert(response.status == 200, "Request to update contact address with all the fields is failing");
        deepEqual(actual, updateContact.expected.response);
    });

    //Test 4
    it('update contact method', async function () {
        let response = {};
        let actual;
        var contactMethod = {
            "email": [{ "value": "ss@ss.com", "isPrimary": true }],
            "phone": [{ "value": "111-111-1111", "isPrimary": true }],
            "fax": [{ "value": "111-111-1111", "isPrimary": true }]
        };
        try {
            //update company, position and name.
            contact.contactMethod = contactMethod;

            response = await axios.post(`${server}/${clientKey}/package/powerhub/contacts/api/update/contact.js`, contact);
            actual = await removePropFromJSON(response.data, ["_vendorKey", "_clientKey", "_ownerKey", "_dateTimeCreated", "_createdBy", "_dateTimeModified", "_modifiedBy", "_signature"]);
            id = response.data._id
        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;
        }
        //update values in expected response to match with actual response
        updateContact.expected.withUpdatedContactMethod["_id"] = id;
        //Verify the response

        assert(response.status == 200, "Request to update  contact method with all the fields is failing");
        deepEqual(actual, updateContact.expected.withUpdatedContactMethod);
    });


});