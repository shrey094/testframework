var createContact = {

    validRequest: {

        allParameters: {
            "_objectType": "contact",
            "type": "individual",
            "name": {
                "firstName": "Test1234",
                "lastName": "Shah",
                "fullName": "Test1234 Shah"
            },
            "groups": [],
            "company": [],
            "isGlobal": false,
            "position": null,
            "address": {
                "addressLine": "390 Ontario Cir.",
                "city": "Boston",
                "postalCode": "02130",
                "state": "Massachusetts",
                "country": "us"
            },
            "contactMethod": null,
            "links": []
        },
        withProjectLinks: {
            "_id": "",
            "_objectType": "contact",
            "type": "individual",
            "name": {
                "firstName": "Test1234",
                "lastName": "Shah",
                "fullName": "Test1234 Shah"
            },
            "groups": [],
            "company": [],
            "isGlobal": false,
            "position": null,
            "address": {
                "addressLine": "390 Ontario Cir.",
                "city": "Boston",
                "postalCode": "02130",
                "state": "Massachusetts",
                "country": "us"
            },
            "contactMethod": null,
            "links": [{
                "_id": "",
                "_objectType": "project",
                "projectKey": "",
                "name": "Test Project Create",
                "_relatedTo": "",
                "_isDirect": true,
            }]

        },
        globalContact: {
            "_objectType": "contact",
            "type": "individual",
            "name": {
                "firstName": "Test1234",
                "lastName": "Shah",
                "fullName": "Test1234 Shah"
            },
            "groups": [],
            "company": [],
            "isGlobal": true,
            "position": null,
            "address": {
                "addressLine": "390 Ontario Cir.",
                "city": "Boston",
                "postalCode": "02130",
                "state": "Massachusetts",
                "country": "us"
            },
            "contactMethod": null,
            "links": []
        },
    },


    expected: {
        response: {
            _id: '5fd7b950c912a333d0ee52bc',
            _objectType: 'contact',
            type: 'individual',
            name: {
                firstName: 'Test1234',
                lastName: 'Shah',
                fullName: 'Test1234 Shah'
            },
            groups: [],
            company: [],
            isGlobal: false,
            position: null,
            address: {
                addressLine: '390 Ontario Cir.',
                city: 'Boston',
                postalCode: '02130',
                state: 'Massachusetts',
                country: 'us'
            },
            contactMethod: null,
            links: []
        },

        withResponseGlobalContact: {
            "_id": "",
            "_objectType": "contact",
            "type": "individual",
            "name": {
                "firstName": "Test1234",
                "lastName": "Shah",
                "fullName": "Test1234 Shah"
            },
            "groups": [],
            "company": [],
            "isGlobal": true,
            "position": null,
            "address": {
                "addressLine": "390 Ontario Cir.",
                "city": "Boston",
                "postalCode": "02130",
                "state": "Massachusetts",
                "country": "us"
            },
            "contactMethod": null,
            "links": []
        },

        bodyWithProjectLinks: {
            _id: '5fd8e0033434aa11342d28cc',
            _objectType: 'contact',
            type: 'individual',
            name: {
                firstName: 'Test1234',
                lastName: 'Shah',
                fullName: 'Test1234 Shah'
            },
            groups: [],
            company: [],
            isGlobal: false,
            position: null,
            address: {
                addressLine: '390 Ontario Cir.',
                city: 'Boston',
                postalCode: '02130',
                state: 'Massachusetts',
                country: 'us'
            },
            contactMethod: null,
            links: [
                {
                    _id: '5fd8dffd3434aa11342d28b1',
                    _objectType: 'project',
                    name: 'test',
                    projectKey: 'projectcreate-kjb26',
                    _relatedTo: '5fd8e0033434aa11342d28cc',
                    _isDirect: true
                }
            ]
        }
    }
};

/*
 *   Retrieve contact Data
 */
var retrieveContact = {
    expectedResponse: {
        "_id": "",
        "_objectType": "contact",
        "type": "individual",
        "name": {
            "firstName": "Test1234",
            "lastName": "Shah",
            "fullName": "Test1234 Shah"
        },
        "groups": [],
        "company": [],
        "isGlobal": false,
        "position": null,
        "address": {
            "addressLine": "390 Ontario Cir.",
            "city": "Boston",
            "postalCode": "02130",
            "state": "Massachusetts",
            "country": "us"
        },
        "contactMethod": null,
        "links": []
    }
};

/*
 *   Delete contact Data
 */

var deleteContact = {

    expectedResponse: {
        "deleted": 1
    }
};

/*
 *   update contact Data
 */
var updateContact = {

    expected: {
        response: {
            "_id": "",
            "_objectType": "contact",
            "type": "individual",
            "name": {
                "firstName": "Test1234",
                "lastName": "Shah",
                "fullName": "Test1234 Shah"
            },
            "groups": [],
            "company": [],
            "isGlobal": false,
            "position": null,
            "address": {
                "addressLine": "390 Ontario Cir.",
                "city": "Boston",
                "postalCode": "02130",
                "state": "Massachusetts",
                "country": "us"
            },
            "contactMethod": null,
            "links": []
        },

        withUpdatedContactMethod: {
            "_id": "",
            "_objectType": "contact",
            "type": "individual",
            "name": {
                "firstName": "Test1234",
                "lastName": "Shah",
                "fullName": "Test1234 Shah"
            },
            "groups": [],
            "company": [],
            "isGlobal": false,
            "position": null,
            "address": {
                "addressLine": "390 Ontario Cir.",
                "city": "Boston",
                "postalCode": "02130",
                "state": "Massachusetts",
                "country": "us"
            },
            "contactMethod": {
                "email": [{
                    "value": "ss@ss.com",
                    "isPrimary": true
                }],
                "phone": [{
                    "value": "111-111-1111",
                    "isPrimary": true
                }],
                "fax": [{
                    "value": "111-111-1111",
                    "isPrimary": true
                }]
            },
            "links": []
        },
        withUpdatedCompany: {
            "_id": "",
            "_objectType": "contact",
            "type": "individual",
            "name": {
                "firstName": "Test1234",
                "lastName": "Shah",
                "fullName": "Test1234 Shah"
            },
            "groups": [],
            "company": [{
                "_id": "companyId",
                "_objectType": "contact",
                "name": ""
            }],
            "isGlobal": false,
            "position": null,
            "address": {
                "addressLine": "390 Ontario Cir.",
                "city": "Boston",
                "postalCode": "02130",
                "state": "Massachusetts",
                "country": "us"
            },
            "contactMethod": null,
            "links": []
        },
        withUpdatedName: {
            "_id": "",
            "_objectType": "contact",
            "type": "individual",
            "name": {
                "firstName": "Updated",
                "lastName": "name",
                "fullName": "Updated name"
            },
            "groups": [],
            "company": [],
            "isGlobal": false,
            "position": "Tester",
            "address": {
                "addressLine": "390 Ontario Cir.",
                "city": "Boston",
                "postalCode": "02130",
                "state": "Massachusetts",
                "country": "us"
            },
            "contactMethod": null,
            "links": []
        },

    },


};

var createProject = {
    input: {
        "_objectType": "project",
        "name": "test",
        "projectKey": "test",
        "type": "biomass",
        "size": {
            "value": null,
            "unit": "kW"
        },
        "startDate": null,
        "status": null,
        "description": null,
        "address": {
            "addressLine": null,
            "city": null,
            "postalCode": null,
            "state": null,
            "country": null
        },
        "geoLocation": {
            "type": "Point",
            "coordinates": [

            ]
        },
        "siteInfo": {
            "siteAccess": {
                "lessor": [

                ]
            },
            "siteOwnership": {
                "owner": [

                ]
            }
        },
        "businessDays": [
            "monday",
            "tuesday",
            "wednesday",
            "thursday",
            "friday"
        ],
        "links": [

        ]
    },
    expected: {
        "_objectType": "project",
        "projectKey": "test",
        "name": "Test Project Create",
        "description": "Create Works!",
        "type": "solar",
        "startDate": "2017-02-12T00:00:00.000Z",
        "address": {
            "addressLine": "390 Ontario Cir.",
            "city": "Boston",
            "postalCode": "02130",
            "state": "Massachusetts",
            "country": "US"
        },
        "geoLocation": {
            "type": "Point",
            "coordinates": [1, 2]
        },
        "size": {
            "value": 2500,
            "unit": "kW"
        },
        "keyContact": [{
            "_objectType": "contact",
            "_id": "5ed918121896bfd3b3f85725",
            "name": "Linda Leader"
        }],
        "owner": [{
            "_objectType": "contact",
            "_id": "5ed9195c1896bfd3b3f8573b",
            "name": "Solra Power, Inc."
        }],
        "omOperator": [{
            "_objectType": "contact",
            "_id": "5ed91a4c1896bfd3b3f8574d",
            "name": "Warm Sun O&M"
        }],
        "interconnection": [{
            "_objectType": "contact",
            "_id": "5ed91a581896bfd3b3f8574e",
            "name": "Summer InterConnectors"
        }],
        "utility": [{
            "_objectType": "contact",
            "_id": "5ed91a661896bfd3b3f8574f",
            "name": "Eversource"
        }],
        "offtaker": [{
            "_objectType": "contact",
            "_id": "5ed91a741896bfd3b3f85750",
            "name": "Tradeenergy America"
        }],
        "status": "operational",
        "businessDays": ["monday", "tuesday"],
        "financialYearEnd": 12,
        "attachment": [],
        "timezone": {
            "region": null,
            "city": null,
            "databaseName": null
        },
        "siteInfo": {
            "siteAccess": {
                "instructions": "Access to the site is through the entrance located at South-East corner. Gate Access Code: 94413",
                "lessor": [{
                    "_id": "5ed9195c1896bfd3b3f8573b",
                    "_objectType": "contact",
                    "name": "Solra Power, Inc."
                }],
                "leaseTermMonth": 60,
                "leaseStartDate": "2016-04-01T04:00:00.000Z",
                "leaseEndDate": "2021-03-31T04:00:00.000Z"
            },
            "siteOwnership": {
                "owner": [{
                    "_id": "5ed9195c1896bfd3b3f8573b",
                    "_objectType": "contact",
                    "name": "Solra Power, Inc."
                }],
                "securityGateCode": "94413"
            }
        },
        "links": []
    }
}

var createCompany = {

    validRequest: {

        allParameters: {
            "_objectType": "contact",
            "type": "company",
            "name": {
                "fullName": "Test1234 Shah"
            },
            "keyContact": [],
            "groups": [],
            "isGlobal": false,
            "address": {
                "addressLine": "390 Ontario Cir.",
                "city": "Boston",
                "postalCode": "02130",
                "state": "Massachusetts",
                "country": "us"
            },
            "contactMethod": null,
            "website": null,
            "links": null
        }
    },

    response: {

        expected: {
            "_id": "",
            "_objectType": "contact",
            "type": "company",
            "name": {
                "fullName": "Test1234 Shah"
            },
            "keyContact": [],
            "groups": [],
            "isGlobal": false,
            "address": {
                "addressLine": "390 Ontario Cir.",
                "city": "Boston",
                "postalCode": "02130",
                "state": "Massachusetts",
                "country": "us"
            },
            "contactMethod": null,
            "website": null,
            "links": null
        }
    }
}

module.exports.testData = {
    createContact: createContact,
    retrieveContact: retrieveContact,
    deleteContact: deleteContact,
    updateContact: updateContact,
    createProject: createProject,
    createCompany: createCompany
};