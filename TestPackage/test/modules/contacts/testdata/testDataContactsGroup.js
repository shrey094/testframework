var createGroup = {
    validRequest: {
        "_objectType": "group",
        "groupKey": "",
        "name": "Contact group test",
        "description": null
    },
    expectedResponse: {
        "_id": "",
        "_objectType": "group",
        "groupKey": "",
        "name": "Contact group test",
        "description": null
    }
};

/*
    *Retrieve group
*/
var retrieveGroup = {
    validRequest: {
        "query": {
            "_objectType": "group",
            "_id": ""
        }
    },
    expectedResponse: {
        "_id": "",
        "_objectType": "group",
        "groupKey": "cexecutive",
        "name": "C Executive",
        "description": null
    }
};

/*
    *Update group
*/
var updateContactGroup = {
    validRequestBody:
    {
        "_id": "5fa3120ee429ad3718db38f0",
        "_objectType": "group",
        "groupKey": "abcdsd",
        "name": "abc123456",
        "description": null
    },

    expectedResponse: {
        "_id": "5fa3120ee429ad3718db38f0",
        "_objectType": "group",
        "groupKey": "abcdsd",
        "name": "abc123456",
        "description": null
    }
};

module.exports.testData = {
    createGroup: createGroup,
    retrieveGroup: retrieveGroup,
    updateContactGroup: updateContactGroup

};