process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0;
const { axios, assert } = require('../../../lib/mochalibs').mochalibs;
const { server, clientKey } = require('../../../lib/dbconfig').dbconfig;
const { createGroup } = require('../testdata/testDataContactsGroup').testData;
const { deepEqual } = require('../../../utils/mochautils').mochautils;

var id;
var groupKey = "contactGroup-" + Math.random().toString(36).substring(2, 7);

describe('delete contact group tests', async function () {
    // Create a contact group to delete before running delete scenarios
    beforeEach('create a group contact', async function () {
        let response = {};
        try {
            createGroup.validRequest.groupKey = groupKey;
            response = await axios.post(`${server}/${clientKey}/package/powerhub/contacts/api/create/group.js`, createGroup.validRequest);
            id = response.data._id;
        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;
        }
        //Verify the response
        assert(response.status == 200, `Request to create a group contact with all the fields is failing : ${response.status}`);
    });


    //test 1
    it('Verify delete contact group with valid values in all the fields (Rule: deletecontactgroup.js)', async function () {
        let response = {};
        let expectedResponse = { "deleted": 1 };
        try {
            response = await axios.post(`${server}/${clientKey}/package/powerhub/contacts/api/delete/group.js`, {
                "_id": id
            });

        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;

        }
        //Verify the test
        assert(response.status == 200, "Status code assertion failed.");
        deepEqual(response.data, expectedResponse);
    });
});