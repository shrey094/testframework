process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0;
const { axios, assert } = require('../../../lib/mochalibs').mochalibs;
const { server, clientKey } = require('../../../lib/dbconfig').dbconfig;
const { createGroup } = require('../testdata/testDataContactsGroup').testData;
const { removePropFromJSON, deepEqual } = require('../../../utils/mochautils').mochautils;

var groupKey = "contactgroup-" + Math.random().toString(36).substring(2, 7);
var id;

describe('Contact  Group Create tests', async function () {

    // Test 1 With a valid body
    it('Verify create group contact with valid values in all the fields', async function () {
        let response = {};
        let actual;
        try {
            createGroup.validRequest.groupKey = groupKey;
            response = await axios.post(`${server}/${clientKey}/package/powerhub/contacts/api/create/group.js`, createGroup.validRequest);
            id = response.data._id;

            //  Removing the property to make the response object same as input object for comparison.
            actual = await removePropFromJSON(response.data, ["_vendorKey", "_clientKey", "_ownerKey", "_dateTimeCreated", "_createdBy", "_dateTimeModified", "_modifiedBy", "_signature"])

        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;
        }

        //update values in expected response to match with actual response
        createGroup.expectedResponse._id = id;
        createGroup.expectedResponse.groupKey = createGroup.validRequest.groupKey;

        //Verify the response
        assert(response.status == 200, `Request to create a group contact with all the fields is failing: ${response.status} `);
        deepEqual(actual, createGroup.expectedResponse);
    });

    // Test 2 With a duplicate key
    it('Verify create group contact with used groupkey', async function () {
        var response = {}
        var response1 = {};
        try {

            createGroup.validRequest.groupKey = "contactgroup-" + Math.random().toString(36).substring(2, 7);
            response1 = await axios.post(`${server}/${clientKey}/package/powerhub/contacts/api/create/group.js`, createGroup.validRequest);

            //provide a used group key
            createGroup.validRequest.groupKey = response1.data.groupKey;
            response = await axios.post(`${server}/${clientKey}/package/powerhub/contacts/api/create/group.js`, createGroup.validRequest);
        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;
        }
        //Verify the response
        assert(response.status == 500, `Request to create a group with duplicate group key is not failing : ${response.status} `);
        deepEqual(response.data, `A group with key '${response1.data.groupKey}' already exist`);
    });


    // Test 3 Without group key
    it('Verify create group contact without groupKey', async function () {
        let response = {};
        let expected = { "groupKey": "must be present" }
        try {
            let request = await removePropFromJSON(createGroup.validRequest, ["groupKey"])
            response = await axios.post(`${server}/${clientKey}/package/powerhub/contacts/api/create/group.js`, request);

        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;
        }
        //Verify the response
        assert(response.status == 400, `Request to create a group contact without groupKey is not failing: ${response.status} `);
        deepEqual(response.data, expected);
    });

    // Test 4 Without name
    it('Verify create group contact without name', async function () {
        let response = {};
        let expected = { "name": "must be present" };
        let request;
        try {
            request = await removePropFromJSON(createGroup.validRequest, ["name"]);
            request.groupKey = "contactgroup-" + Math.random().toString(36).substring(2, 7);
            response = await axios.post(`${server}/${clientKey}/package/powerhub/contacts/api/create/group.js`, request);
        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;
        }
        //Verify the response
        assert(response.status == 400, `Request to create a group contact without name is not failing: ${response.status} `);
        deepEqual(response.data, expected);
    });

});  