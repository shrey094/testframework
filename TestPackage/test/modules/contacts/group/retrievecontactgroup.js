process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0;
const { axios, assert } = require('../../../lib/mochalibs').mochalibs;
const { server, clientKey } = require('../../../lib/dbconfig').dbconfig;
const { createGroup, retrieveGroup } = require('../testdata/testDataContactsGroup').testData;
const { removePropFromJSON, deepEqual } = require('../../../utils/mochautils').mochautils;

var id;
var groupKey = Math.random().toString(36).substring(2, 7);

describe('Contact  Group retrieve tests', async function () {

    // Create a contact to retrieve before running delete scenarios
    beforeEach('create a group contact ', async function () {
        let response = {};
        let actual;
        try {
            createGroup.validRequest.groupKey = groupKey;
            response = await axios.post(`${server}/${clientKey}/package/powerhub/contacts/api/create/group.js`, createGroup.validRequest);
            id = response.data._id;

            //  Removing the property to make the response object same as input object for comparison.
            actual = await removePropFromJSON(response.data, ["_vendorKey", "_clientKey", "_ownerKey", "_dateTimeCreated", "_createdBy", "_dateTimeModified", "_modifiedBy", "_signature"])

        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;
        }

        //update values in expected response to match with actual response
        createGroup.expectedResponse._id = id;
        createGroup.expectedResponse.groupKey = createGroup.validRequest.groupKey;

        //Verify the response
        assert(response.status == 200, "Request to create a group contact with all the fields is failing");
        deepEqual(actual, createGroup.expectedResponse);
    });

    // Test 1 Retrieve a contact
    it('Verify retrieve contact with valid request body (Rule: retrievecontactgroup.js)', async function () {
        let response = {};
        let actual;

        try {

            retrieveGroup.validRequest.query._id = id;
            response = await axios.post(`${server}/${clientKey}/package/powerhub/contacts/api/retrieve/groups.js`, retrieveGroup.validRequest);

            //Removing the property to make the response object same as input object for comparison.
            actual = await removePropFromJSON(response.data.data[0], ["_vendorKey", "_clientKey", "_ownerKey", "_dateTimeCreated", "_createdBy", "_dateTimeModified", "_modifiedBy", "_signature"]);

        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;
        }

        //update values in expected response to match with actual response
        retrieveGroup.expectedResponse._id = id;
        retrieveGroup.expectedResponse.groupKey = actual.groupKey;
        retrieveGroup.expectedResponse.name = actual.name;

        //Verify the response
        assert(response.status == 200, "Request to retrieve contact group with all the fields is failing");
        deepEqual(actual, retrieveGroup.expectedResponse); //Verify the whole object
    });
});