process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0;
const { axios, assert } = require('../../../lib/mochalibs').mochalibs;
const { server, clientKey } = require('../../../lib/dbconfig').dbconfig;
const { createGroup, updateContactGroup } = require('../testdata/testDataContactsGroup').testData;
const { removePropFromJSON, deepEqual } = require('../../../utils/mochautils').mochautils;

let id;
let group;
var groupKey = "GroupKey" + Math.random().toString(36).substring(2, 7);

describe('Contact update Group tests', async function () {


    // Create a contact to retrieve before running delete scenarios
    beforeEach('create a group contact', async function () {
        let response = {};
        try {

            createGroup.validRequest.groupKey = groupKey;
            response = await axios.post(`${server}/${clientKey}/package/powerhub/contacts/api/create/group.js`, createGroup.validRequest);
            id = response.data._id;

            //  Removing the property to make the response object same as input object for comparison.
            group = await removePropFromJSON(response.data, ["_vendorKey", "_clientKey", "_ownerKey", "_dateTimeCreated", "_createdBy", "_dateTimeModified", "_modifiedBy", "_signature"])

        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;
        }

        //update values in expected response to match with actual response
        createGroup.expectedResponse._id = id;
        createGroup.expectedResponse.groupKey = createGroup.validRequest.groupKey;

        //Verify the response
        assert(response.status == 200, "Request to create a group contact with all the fields is failing");
    });


    // // Test 1 Update group name
    // it('Verify update contact group name with valid values in all the fields', async function () {
    //     let response = {};
    //     let actual;

    //     try {
    //         //update group name 
    //         group.name = "Updated name";
    //         response = await axios.post(`${server}/${clientKey}/package/powerhub/contacts/api/update/group.js`, group);
    //         id = response.data._id;

    //         //  Removing the property to make the response object same as input object for comparison.
    //         actual = await removePropFromJSON(response.data, ["_vendorKey", "_clientKey", "_ownerKey", "_dateTimeCreated", "_createdBy", "_dateTimeModified", "_modifiedBy", "_signature"])

    //     } catch (e) {
    //         response.status = e.response.status;
    //         response.data = e.response.data;
    //     }

    //     // updated expected response to match with actual
    //     updateContactGroup.expectedResponse._id = id;
    //     updateContactGroup.expectedResponse.name = actual.name;
    //     updateContactGroup.expectedResponse.groupKey = actual.groupKey;

    //     //Verify the response object
    //     assert(response.status == 200, "Request to update contact group name with valid values in all the fields is failing");
    //     deepEqual(actual, updateContactGroup.expectedResponse); //Verify the whole object
    // });


    // Test 2 Update group without name and key 
    it('Verify update contact group Update group without name and key', async function () {
        let response = {};
        let actual;
        let request = {};
        try {
            //Remove group name and key 
            request = await removePropFromJSON(group, ["name", "groupKey"])
            response = await axios.post(`${server}/${clientKey}/package/powerhub/contacts/api/update/group.js`, request);
            id = response.data._id;
            //  Removing the property to make the response object same as input object for comparison.
            actual = await removePropFromJSON(response.data, ["_vendorKey", "_clientKey", "_ownerKey", "_dateTimeCreated", "_createdBy", "_dateTimeModified", "_modifiedBy", "_signature"])
        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;
        }
        // updated expected response to match with actual
        updateContactGroup.expectedResponse._id = id;
        updateContactGroup.expectedResponse.name = actual.name;
        updateContactGroup.expectedResponse.groupKey = actual.groupKey;

        //Verify the response object
        assert(response.status == 200, "Request to update contact group name with valid values in all the fields is failing");
        deepEqual(actual, updateContactGroup.expectedResponse); //Verify the whole object
    });

});