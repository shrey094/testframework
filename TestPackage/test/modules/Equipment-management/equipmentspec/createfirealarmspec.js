process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0;
const { axios, assert, ObjectId } = require('../../../lib/mochalibs').mochalibs;
const { server, clientKey } = require('../../../lib/dbconfig').dbconfig;
const { createEquipmentSpec } = require('../testdata/testdataequipments').testData;
const { removePropFromJSON, deepEqual } = require('../../../utils/mochautils').mochautils;
const { deleteOne } = require('../../../utils/dbwrapper').dbwrapper;
var id;


describe('equipment spec create tests', async function () {

    afterEach('delete equipment spec', async function () {
        await deleteOne("equipment", { "_objectType": "equipmentSpec", "_id": ObjectId(id) });
    });

    it('create equipment Spec', async function () {
        let response = {};
        try {
            response = await axios.post(`${server}/${clientKey}/package/powerhub/equipment/api/create/equipmentspec.js`, createEquipmentSpec.Request.FE);
            id = response.data._id;
            var actual = await removePropFromJSON(response.data, ["_vendorKey", "_clientKey", "_ownerKey", "_dateTimeCreated", "_createdBy", "_dateTimeModified", "_modifiedBy", "_signature"]);

        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;
        }
        createEquipmentSpec.Response.FE._id = id;
        assert(response.status == 200, `Request to create Fire alarm equipment spec with all the fields is failing: ${response.status} `);
        deepEqual(actual, createEquipmentSpec.Response.FE);
    });
});