var startDate = "01/19/2021";
var startDateResponse = "2021-01-19T00:00:00.000Z";

var createEquipment = {
    Request: {
        allParameters: {

            "_objectType": "equipment",
            "type": "fire",
            "_count": {
                "ticket": 0
            },
            "typeCaption": "Fire Alarm & Extinguisher",
            "name": "shrey",
            "deviceId": "1234",
            "serialNumber": "123",
            "componentType": "11",
            "lifeCycleStatus": "installed",
            "dateInstalled": null,
            "datePurchased": "01/11/2021",
            "commissioningDate": "01/21/2021",
            "constructionDate": "01/30/2021",
            "constructionCompany": "123",
            "takeoverDate": null,
            "geoLocation": {
                "type": "Point",
                "coordinates": [
                    "11",
                    "11"
                ]
            },
            "additionalInforamtion": "desc",
            "warranty": {
                "status": null,
                "startDate": null,
                "endDate": null,
                "length": null,
                "unit": "month"
            },
            "equipmentSpec": {
                "_id": "6001e8c40f4d9320d85df131"
            },
            "inventoryItem": {
                "_id": null
            },
            "links": [
                {
                    "_objectType": "equipment",
                    "_id": "6001e8c40f4d9320d85df131"
                },
                {
                    "_objectType": "project",
                    "_id": "6000af98fe1e9d3820dd9256"
                }
            ]
        },
    },

    Response: {
        allParameters: {
            "_id": "6004630b0f4d9320d85f562c",
            "_objectType": "equipment",
            "type": "fire",
            "_count": {
                "ticket": 0
            },
            "typeCaption": "Fire Alarm & Extinguisher",
            "name": "shrey",
            "deviceId": "1234",
            "serialNumber": "123",
            "componentType": "11",
            "lifeCycleStatus": "installed",
            "dateInstalled": null,
            "datePurchased": "2021-01-11T00:00:00.000Z",
            "commissioningDate": "2021-01-21T00:00:00.000Z",
            "constructionDate": "2021-01-30T00:00:00.000Z",
            "constructionCompany": "123",
            "takeoverDate": null,
            "geoLocation": {
                "type": "Point",
                "coordinates": [
                    "11",
                    "11"
                ]
            },
            "additionalInforamtion": "desc",
            "warranty": {
                "status": null,
                "startDate": null,
                "endDate": null,
                "length": null,
                "unit": "month"
            },
            "equipmentSpec": {
                "_id": "",
                "_objectType": "equipmentSpec",
                "name": "FE_SHREY",
                "type": "fire",
                "manufacturer": "shrey",
                "modelNo": "1234"
            },
            "inventoryItem": [],
            "links": [
                {
                    "_id": "",
                    "_objectType": "project",
                    "_relatedTo": "",
                    "name": "test",
                    "projectKey": "",
                    "_isDirect": true
                }
            ]
        },
    }
};
var retrieveEquipment = {
    Request: {
        allParameters: {
            "_id": "6001e9150f4d9320d85df4ea"
        },
    },

    Response: {
        allParameters: {
            "_id": "",
            "_objectType": "equipment",
            "type": "fire",
            "_count": {
                "ticket": 0
            },
            "typeCaption": "Fire Alarm & Extinguisher",
            "name": "shrey",
            "deviceId": "1234",
            "serialNumber": "123",
            "componentType": "11",
            "lifeCycleStatus": "installed",
            "dateInstalled": null,
            "datePurchased": "2021-01-11T00:00:00.000Z",
            "commissioningDate": "2021-01-21T00:00:00.000Z",
            "constructionDate": "2021-01-30T00:00:00.000Z",
            "constructionCompany": "123",
            "takeoverDate": null,
            "geoLocation": {
                "type": "Point",
                "coordinates": [
                    "11",
                    "11"
                ]
            },
            "additionalInforamtion": "desc",
            "warranty": {
                "status": null,
                "startDate": null,
                "endDate": null,
                "length": null,
                "unit": "month"
            },
            "equipmentSpec": {
                "_id": "",
                "_objectType": "equipmentSpec",
                "name": "FE_SHREY",
                "type": "fire",
                "manufacturer": "shrey",
                "modelNo": "1234"
            },
            "inventoryItem": [],
            "links": [
                {
                    "_id": "",
                    "_objectType": "project",
                    "_relatedTo": "",
                    "name": "test",
                    "projectKey": "",
                    "_isDirect": true
                }
            ]
        },

    }
};
var updateEquipment = {
    Request: {
        update_name_deviceId: {
            "_id": "",
            "_objectType": "equipment",
            "type": "fire",
            "_count": {
                "ticket": 0
            },
            "typeCaption": "Fire Alarm & Extinguisher",
            "name": "shrey test",
            "deviceId": "123456779",
            "serialNumber": "123",
            "componentType": "11",
            "lifeCycleStatus": "installed",
            "dateInstalled": null,
            "datePurchased": "01/11/2021",
            "commissioningDate": "01/21/2021",
            "constructionDate": "01/30/2021",
            "constructionCompany": "123",
            "takeoverDate": null,
            "geoLocation": {
                "type": "Point",
                "coordinates": [
                    "11",
                    "11"
                ]
            },
            "additionalInforamtion": "desc",
            "warranty": {
                "status": null,
                "startDate": null,
                "endDate": null,
                "length": null,
                "unit": "month"
            },
            "equipmentSpec": {
                "_id": ""
            },
            "inventoryItem": {
                "_id": null
            },
            "links": [
                {
                    "_objectType": "project",
                    "_id": ""
                }
            ]
        },
    },

    Response: {
        update_name_deviceId: {
            "_id": "",
            "_objectType": "equipment",
            "type": "fire",
            "_count": {
                "ticket": 0
            },
            "typeCaption": "Fire Alarm & Extinguisher",
            "name": "shrey test",
            "deviceId": "123456779",
            "serialNumber": "123",
            "componentType": "11",
            "lifeCycleStatus": "installed",
            "dateInstalled": null,
            "datePurchased": "2021-01-11T00:00:00.000Z",
            "commissioningDate": "2021-01-21T00:00:00.000Z",
            "constructionDate": "2021-01-30T00:00:00.000Z",
            "constructionCompany": "123",
            "takeoverDate": null,
            "geoLocation": {
                "type": "Point",
                "coordinates": [
                    "11",
                    "11"
                ]
            },
            "additionalInforamtion": "desc",
            "warranty": {
                "status": null,
                "startDate": null,
                "endDate": null,
                "length": null,
                "unit": "month"
            },
            "equipmentSpec": {
                "_id": "",
                "_objectType": "equipmentSpec",
                "name": "FE_SHREY",
                "type": "fire",
                "manufacturer": "shrey",
                "modelNo": "1234"
            },
            "inventoryItem": [

            ],
            "links": [
                {
                    "_id": "",
                    "_objectType": "project",
                    "name": "test",
                    "projectKey": "",
                    "_relatedTo": "",
                    "_isDirect": true
                }
            ]
        },

    }
};

var deleteEquipment = {
    Request: {
        allParameters: {
            "_id": "60075b6b0db10223e85f638e",
            "selectedEquipment": {
                "_id": "60075b6b0db10223e85f638e",
                "type": "fire",
                "_count": {
                    "ticket": 0
                },
                "typeCaption": "Fire Alarm & Extinguisher",
                "name": "shrey",
                "serialNumber": "123",
                "lifeCycleStatus": "Installed",
                "datePurchased": "01/11/2021",
                "commissioningDate": "01/21/2021",
                "warranty": {
                    "status": null,
                    "startDate": null,
                    "endDate": null,
                    "length": null,
                    "unit": "month"
                },
                "equipmentSpec": {
                    "_id": "60075b6a0db10223e85f637b",
                    "_objectType": "equipmentSpec",
                    "name": "FE_SHREY",
                    "type": "fire",
                    "manufacturer": "shrey",
                    "modelNo": "1234"
                },
                "inventoryItem": [],
                "links": [],
                "typevalue": "fire",
                "equipmentSpec.modelNo": "fire- 56922806",
                "equipmentSpec.manufacturer": "fire Manufacturer",
                "warranty.startDate": "",
                "warranty.endDate": "",
                "warranty.status": "",
                "links.name": null,
                "countTickets": 0
            }
        }
    }
}

var createEquipmentSpec = {
    Request: {
        FE: {
            "_objectType": "equipmentSpec",
            "_count": 0,
            "name": "FE_SHREY",
            "type": "fire",
            "typeCaption": "Fire Alarm & Extinguisher",
            "manufacturer": "shrey",
            "modelNo": "1234",
            "maxDcVoltage": 12,
            "maxAcPower": 12,
            "country": "us",
            "specification": null,
            "capacity": null
        }
    },

    Response: {
        FE: {
            "_id": "600830960db10223e85fe8cf",
            "_objectType": "equipmentSpec",
            "_count": 0,
            "name": "FE_SHREY",
            "type": "fire",
            "typeCaption": "Fire Alarm & Extinguisher",
            "manufacturer": "shrey",
            "modelNo": "1234",
            "maxDcVoltage": 12,
            "maxAcPower": 12,
            "country": "us",
            "specification": null,
            "capacity": null,
            "price": null,
            "address": null
        }
    }
};


var retrieveSpec = {
    Response: {
        FE: {
            "_id": "600830960db10223e85fe8cf",
            "_objectType": "equipmentSpec",
            "_count": 0,
            "name": "FE_SHREY",
            "type": "fire",
            "typeCaption": "Fire Alarm & Extinguisher",
            "manufacturer": "shrey",
            "modelNo": "1234",
            "maxDcVoltage": 12,
            "maxAcPower": 12,
            "country": "us",
            "specification": null,
            "capacity": null,
            "price": null,
            "address": null
        }
    }
}


var createProject = {
    input: {
        "_objectType": "project",
        "name": "test",
        "projectKey": "test",
        "type": "biomass",
        "size": {
            "value": null,
            "unit": "kW"
        },
        "startDate": startDate,
        "status": null,
        "description": null,
        "address": {
            "addressLine": null,
            "city": null,
            "postalCode": null,
            "state": null,
            "country": null
        },
        "geoLocation": {
            "type": "Point",
            "coordinates": [

            ]
        },
        "siteInfo": {
            "siteAccess": {
                "lessor": [

                ]
            },
            "siteOwnership": {
                "owner": [

                ]
            }
        },
        "businessDays": [
            "monday",
            "tuesday",
            "wednesday",
            "thursday",
            "friday"
        ],
        "links": [

        ]
    },
    expected: {
        "_objectType": "project",
        "projectKey": "test",
        "name": "Test Project Create",
        "description": "Create Works!",
        "type": "solar",
        "startDate": startDateResponse,
        "address": {
            "addressLine": "390 Ontario Cir.",
            "city": "Boston",
            "postalCode": "02130",
            "state": "Massachusetts",
            "country": "US"
        },
        "geoLocation": {
            "type": "Point",
            "coordinates": [1, 2]
        },
        "size": {
            "value": 2500,
            "unit": "kW"
        },
        "keyContact": [{
            "_objectType": "contact",
            "_id": "5ed918121896bfd3b3f85725",
            "name": "Linda Leader"
        }],
        "owner": [{
            "_objectType": "contact",
            "_id": "5ed9195c1896bfd3b3f8573b",
            "name": "Solra Power, Inc."
        }],
        "omOperator": [{
            "_objectType": "contact",
            "_id": "5ed91a4c1896bfd3b3f8574d",
            "name": "Warm Sun O&M"
        }],
        "interconnection": [{
            "_objectType": "contact",
            "_id": "5ed91a581896bfd3b3f8574e",
            "name": "Summer InterConnectors"
        }],
        "utility": [{
            "_objectType": "contact",
            "_id": "5ed91a661896bfd3b3f8574f",
            "name": "Eversource"
        }],
        "offtaker": [{
            "_objectType": "contact",
            "_id": "5ed91a741896bfd3b3f85750",
            "name": "Tradeenergy America"
        }],
        "status": "operational",
        "businessDays": ["monday", "tuesday"],
        "financialYearEnd": 12,
        "attachment": [],
        "timezone": {
            "region": null,
            "city": null,
            "databaseName": null
        },
        "siteInfo": {
            "siteAccess": {
                "instructions": "Access to the site is through the entrance located at South-East corner. Gate Access Code: 94413",
                "lessor": [{
                    "_id": "5ed9195c1896bfd3b3f8573b",
                    "_objectType": "contact",
                    "name": "Solra Power, Inc."
                }],
                "leaseTermMonth": 60,
                "leaseStartDate": "2016-04-01T04:00:00.000Z",
                "leaseEndDate": "2021-03-31T04:00:00.000Z"
            },
            "siteOwnership": {
                "owner": [{
                    "_id": "5ed9195c1896bfd3b3f8573b",
                    "_objectType": "contact",
                    "name": "Solra Power, Inc."
                }],
                "securityGateCode": "94413"
            }
        },
        "links": []
    }
};

var createProjectGroup = {
    Request: {
        allParameters: {
            "_objectType": "projectGroup",
            "projectGroupKey": "test1234d",
            "name": "test1234d"
        }
    },
    Response: {
        allParameters: {
            "_id": "5fe0f59ad85dc70f547044cc",
            "_objectType": "projectGroup",
            "projectGroupKey": "test1234d",
            "name": "test1234d",
            "_count": {},
            "links": []
        }
    }
}

var createContact = {

    validRequest: {

        allParameters: {
            "_objectType": "contact",
            "type": "individual",
            "name": {
                "firstName": "Test1234",
                "lastName": "Shah",
                "fullName": "Test1234 Shah"
            },
            "groups": [],
            "company": [],
            "isGlobal": false,
            "position": null,
            "address": {
                "addressLine": "390 Ontario Cir.",
                "city": "Boston",
                "postalCode": "02130",
                "state": "Massachusetts",
                "country": "us"
            },
            "contactMethod": null,
            "links": []
        },
    },
    expected: {
        response: {
            _id: '5fd7b950c912a333d0ee52bc',
            _objectType: 'contact',
            type: 'individual',
            name: {
                firstName: 'Test1234',
                lastName: 'Shah',
                fullName: 'Test1234 Shah'
            },
            groups: [],
            company: [],
            isGlobal: false,
            position: null,
            address: {
                addressLine: '390 Ontario Cir.',
                city: 'Boston',
                postalCode: '02130',
                state: 'Massachusetts',
                country: 'us'
            },
            contactMethod: null,
            links: []
        },
    }
};

module.exports.testData = {
    createProject: createProject,
    createProjectGroup: createProjectGroup,
    createContact: createContact,
    createEquipment: createEquipment,
    createEquipmentSpec: createEquipmentSpec,
    retrieveEquipment: retrieveEquipment,
    updateEquipment: updateEquipment,
    deleteEquipment: deleteEquipment,
    retrieveSpec: retrieveSpec

};