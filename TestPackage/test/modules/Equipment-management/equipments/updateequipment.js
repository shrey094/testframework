process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0;
const { axios, assert, ObjectId } = require('../../../lib/mochalibs').mochalibs;
const { server, clientKey } = require('../../../lib/dbconfig').dbconfig;
const { createEquipment, updateEquipment, createEquipmentSpec, createProject } = require('../testdata/testdataequipments').testData;
const { removePropFromJSON, deepEqual } = require('../../../utils/mochautils').mochautils;
const { deleteOne } = require('../../../utils/dbwrapper').dbwrapper;
var id, fireExtinguisherSpecId, equipmentId, project, projectId;


describe('equipment update tests', async function () {
    before('create project and equipment Spec', async function () {
        let response = {};
        try {
            //Create a project
            var projectKey = "projectcreate-" + Math.random().toString(36).substring(2, 7);
            createProject.input.projectKey = projectKey;
            response = await axios.post(`${server}/${clientKey}/package/powerhub/projectportfolio/api/create/project.js`, createProject.input);
            projectId = response.data._id
            project = await removePropFromJSON(response.data, ["_vendorKey", "_clientKey", "_ownerKey", "_dateTimeCreated", "_createdBy", "_dateTimeModified", "_modifiedBy", "_signature"])
            //Create equipment spec
            response = await axios.post(`${server}/${clientKey}/package/powerhub/equipment/api/create/equipmentspec.js`, createEquipmentSpec.Request.FE);
            fireExtinguisherSpecId = response.data._id
        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;
        }
        assert(response.status == 200, `Request to create FE equipment spec with all the fields is failing: ${response.status} `);
    });

    beforeEach('1. create equipment', async function () {
        let response = {};
        try {
            createEquipment.Request.allParameters.equipmentSpec._id = fireExtinguisherSpecId;
            createEquipment.Request.allParameters.links[0]._id = fireExtinguisherSpecId;
            createEquipment.Request.allParameters.links[1]._id = projectId;
            response = await axios.post(`${server}/${clientKey}/package/powerhub/equipment/api/create/equipment.js`, createEquipment.Request.allParameters);
            equipmentId = response.data._id;
        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;
        }
        // Verify if the test was successful.
        assert(response.status == 200, "Request to create equipment with all the fields is failing");
    });

    afterEach('delete equipment, spec', async function () {
        await deleteOne("equipment", { "_objectType": "equipment", "_id": ObjectId(id) });

    });

    after('delete project', async function () {
        await deleteOne("projects", { "_objectType": "project", "_id": ObjectId(projectId) });
        await deleteOne("equipment", { "_objectType": "equipmentSpec", "_id": ObjectId(fireExtinguisherSpecId) });
    });

    it('1. update equipment', async function () {
        let response = {};
        try {
            debugger;
            updateEquipment.Request.update_name_deviceId._id = equipmentId;
            updateEquipment.Request.update_name_deviceId.equipmentSpec._id = fireExtinguisherSpecId;
            updateEquipment.Request.update_name_deviceId.links[0]._id = projectId;
            response = await axios.post(`${server}/${clientKey}/package/powerhub/equipment/api/update/equipment.js`, updateEquipment.Request.update_name_deviceId);
            id = response.data._id;
            var actual = await removePropFromJSON(response.data, ["_vendorKey", "_clientKey", "_ownerKey", "_dateTimeCreated", "_createdBy", "_dateTimeModified", "_modifiedBy", "_signature"])
        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;
            console.log(actual)
        }
        //update values in expected response to match with actual response
        updateEquipment.Response.update_name_deviceId["_id"] = id;
        updateEquipment.Response.update_name_deviceId.equipmentSpec._id = fireExtinguisherSpecId;
        updateEquipment.Response.update_name_deviceId.links[0]._relatedTo = id;
        updateEquipment.Response.update_name_deviceId.links[0]._id = projectId;
        updateEquipment.Response.update_name_deviceId.links[0].projectKey = project.projectKey;
        // Verify if the test was successful.
        assert(response.status == 200, "Request to create equipment with all the fields is failing");
        deepEqual(actual, updateEquipment.Response.update_name_deviceId); //Verify the whole object
    });
});