process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0;
const { axios, assert, ObjectId } = require('../../../lib/mochalibs').mochalibs;
const { server, clientKey } = require('../../../lib/dbconfig').dbconfig;
const { createEquipment, createEquipmentSpec, createProject } = require('../testdata/testdataequipments').testData;
const { removePropFromJSON, deepEqual } = require('../../../utils/mochautils').mochautils;
const { deleteOne } = require('../../../utils/dbwrapper').dbwrapper;
var id, fireExtinguisherSpecId, project, projectId;


describe('equipment create tests', async function () {

    beforeEach('create Spec', async function () {

        let response = {};
        try {
            response = await axios.post(`${server}/${clientKey}/package/powerhub/equipment/api/create/equipmentspec.js`, createEquipmentSpec.Request.FE);
            fireExtinguisherSpecId = response.data._id
        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;
        }
        assert(response.status == 200, `Request to create FE equipment spec with all the fields is failing: ${response.status} `);
    });


    before('create project', async function () {
        let response = {};
        var projectKey = "projectcreate-" + Math.random().toString(36).substring(2, 7);
        createProject.input.projectKey = projectKey;
        try {
            response = await axios.post(`${server}/${clientKey}/package/powerhub/projectportfolio/api/create/project.js`, createProject.input);
            projectId = response.data._id
            project = await removePropFromJSON(response.data, ["_vendorKey", "_clientKey", "_ownerKey", "_dateTimeCreated", "_createdBy", "_dateTimeModified", "_modifiedBy", "_signature"])

        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;

        }
        assert(response.status == 200, `Request to create project with all the fields is failing: ${response.status} `);
    });


    afterEach('delete equipment', async function () {
        await deleteOne("equipment", { "_objectType": "equipment", "_id": ObjectId(id) });
        await deleteOne("equipment", { "_objectType": "equipmentSpec", "_id": ObjectId(fireExtinguisherSpecId) });
    });


    after('delete project', async function () {
        await deleteOne("projects", { "_objectType": "project", "_id": ObjectId(projectId) });
    });



    it('1. create equipment', async function () {
        let response = {};
        try {

            createEquipment.Request.allParameters.equipmentSpec._id = fireExtinguisherSpecId;
            createEquipment.Request.allParameters.links[0]._id = fireExtinguisherSpecId;
            createEquipment.Request.allParameters.links[1]._id = projectId;
            response = await axios.post(`${server}/${clientKey}/package/powerhub/equipment/api/create/equipment.js`, createEquipment.Request.allParameters);
            id = response.data._id;
            var actual = await removePropFromJSON(response.data, ["_vendorKey", "_clientKey", "_ownerKey", "_dateTimeCreated", "_createdBy", "_dateTimeModified", "_modifiedBy", "_signature"])
        } catch (e) {
            response.status = e.response.status;
            response.data = e.response.data;

        }
        //update values in expected response to match with actual response
        createEquipment.Response.allParameters["_id"] = id;
        createEquipment.Response.allParameters.equipmentSpec._id = fireExtinguisherSpecId;
        createEquipment.Response.allParameters.links[0]._relatedTo = id;
        createEquipment.Response.allParameters.links[0]._id = projectId;
        createEquipment.Response.allParameters.links[0].projectKey = project.projectKey;
        // Verify if the test was successful.
        assert(response.status == 200, `Request to create equipment with all the is failing: ${response.status} `);
        deepEqual(actual, createEquipment.Response.allParameters); //Verify the whole object
    });


});